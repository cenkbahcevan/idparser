package NamedEntityRecognizer;

import IDParser.IDParserTools;
import IDParser.IDSentence;
import IDParser.IDWord;
import PosTagger.MarkovModel;

import java.util.*;

/**
 * Created by cenk on 15.04.2018.
 */
public class SentenceFeatures {

    private Set<String> featuresSet;

    private Set<String> classSet;


    private List<List<String>> training_features_X;

    private List<String> training_Y;

    private Map<String,Map<String,Double>> weights;

    private boolean unTrained = false;

    private FileReader f;

    MarkovModel model;

    public SentenceFeatures(FileReader f) {


        this.f = f;

        weights = new HashMap<>();


        this.classSet = new HashSet<>();


        for (IDSentence sentence:f.getFileLines()){

            List<IDWord> wordsOfSentence = sentence.getWordsOfSentence();

            for (int i=0;i<wordsOfSentence.size();i++){

                initializeFeatureClass(sentence,i);

            }

        }

    }

    private void putFeatureIfNotExist(String feature){
        if (!this.weights.containsKey(feature)){
            this.weights.put(feature,new HashMap<>());
        }
    }

    private void increaseFeatureClass(String feature){
        for (String className:this.classSet)
        if (!this.weights.get(feature).containsKey(className)){
            this.weights.get(feature).put(className,0.0);
        }

    }




    private List<String> generateFeatures(IDSentence sentence,int wordNo){
        List<String> resultList = new ArrayList<String>();

        IDWord currentWord = sentence.getWordsOfSentence().get(wordNo);

        String currentWordFirstLetter = currentWord.getWordContent().substring(0,1);
        String currentWordUpperLetter = currentWord.getWordContent().toUpperCase().substring(0,1);

        if (currentWordFirstLetter.equals(currentWordUpperLetter)){

            resultList.add("uppercase");
        }


        if (wordNo == 0) {

            resultList.add("beginning");

        }
        else{
            resultList.add("center");
        }

        String posTagFeature = "postag" +  currentWord.getWordPos();
        resultList.add(posTagFeature);



        String wordRoot =  "wordroot" + currentWord.getWordRoot().toLowerCase();
        resultList.add(wordRoot);


        if (wordNo > 0){

            IDWord beforeWord = sentence.getWordsOfSentence().get(wordNo -1 );

            String beforeWordStr = "word1Before" + beforeWord.getWordRoot();
            resultList.add(beforeWordStr);

        }

        if (wordNo < sentence.getWordsOfSentence().size() -1){
            IDWord nextWord = sentence.getWordsOfSentence().get(wordNo + 1);

            String nextWordStr = "word1Next" + nextWord.getWordRoot();
            resultList.add(nextWordStr);
        }

        if (wordNo < sentence.getWordsOfSentence().size() - 2){
            IDWord nextWord = sentence.getWordsOfSentence().get(wordNo + 2);

            String nextWordStr = "word2Next" + nextWord.getWordRoot();
            resultList.add(nextWordStr);
        }




        return resultList;
    }


    private List<String> initializeFeatureClass(IDSentence sentence,int wordNo){

        List<String> resultList = new ArrayList<String>();

        IDWord currentWord = sentence.getWordsOfSentence().get(wordNo);
        String currentLabel = currentWord.getNerTag();

        String currentWordFirstLetter = currentWord.getWordContent().substring(0,1);
        String currentWordUpperLetter = currentWord.getWordContent().toUpperCase().substring(0,1);

        if (currentWordFirstLetter.equals(currentWordUpperLetter)){

            putFeatureIfNotExist("uppercase");
            increaseFeatureClass("uppercase");
            resultList.add("uppercase");
        }


        if (wordNo == 0) {
            putFeatureIfNotExist("beginning");
            increaseFeatureClass("beginning");
            resultList.add("beginning");

        }


        else if (wordNo > 0){

            IDWord beforeWord = sentence.getWordsOfSentence().get(wordNo -1 );

            String beforeWordStr = "word1Before" + beforeWord.getWordRoot();
            putFeatureIfNotExist(beforeWordStr);
            increaseFeatureClass(beforeWordStr);
            resultList.add(beforeWordStr);

        }


        if (wordNo < sentence.getWordsOfSentence().size() - 1){
            IDWord nextWord = sentence.getWordsOfSentence().get(wordNo + 1);

            String nextWordStr = "word1Next" + nextWord.getWordRoot();
            putFeatureIfNotExist(nextWordStr);
            increaseFeatureClass(nextWordStr);
            resultList.add(nextWordStr);
        }

        if (wordNo < sentence.getWordsOfSentence().size() - 2){
            IDWord nextWord = sentence.getWordsOfSentence().get(wordNo + 2);

            String nextWordStr = "word2Next" + nextWord.getWordRoot();
            putFeatureIfNotExist(nextWordStr);
            increaseFeatureClass(nextWordStr);
            resultList.add(nextWordStr);
        }

        String posTagFeature = "postag" +  currentWord.getWordPos();
        resultList.add(posTagFeature);
        putFeatureIfNotExist(posTagFeature);
        increaseFeatureClass(posTagFeature );


        String wordRoot =  "wordroot" + currentWord.getWordRoot().toLowerCase();
        resultList.add(wordRoot);
        putFeatureIfNotExist(wordRoot);
        increaseFeatureClass(wordRoot);

        classSet.add(currentLabel);

        return resultList;
    }


    public String predict(List<String> features){

        double biggestScore = -1;
        String result = "";

        for (String className:this.classSet){
            double tempScore = 0.0;
            for (String featureName:features){
                if (this.weights.containsKey(featureName)) {
                    tempScore = tempScore + this.weights.get(featureName).get(className);
                }
            }
            if (tempScore>biggestScore){
                biggestScore = tempScore;
                result = className;

            }
        }
        return  result;

    }

    public void update(String correctRes,String wrongRes,List<String> features){

        for (String feature:features){

            double oldValueWrongClass = this.weights.get(feature).get(wrongRes);
            double oldValueTrueClass =  this.weights.get(feature).get(correctRes);

            this.weights.get(feature).put(wrongRes,oldValueWrongClass - 1);
            this.weights.get(feature).put(correctRes,oldValueTrueClass + 1);

        }

    }


    public void train() {

        for (IDSentence sentence : f.getFileLines()) {

            List<IDWord> wordsOfSentence = sentence.getWordsOfSentence();

            for (int i = 0; i < wordsOfSentence.size(); i++) {

                List<String> featureList = initializeFeatureClass(sentence,i);

                String modelResult = predict(featureList);
                String realResult = sentence.getWordsOfSentence().get(i).getNerTag();

                if (!modelResult.equals(realResult)){

                    update(realResult,modelResult,featureList);

                }


            }

        }
    }

    public Map<String, Map<String, Double>> getWeights() {
        return weights;
    }

    public static void main(String[] args) throws Exception {

        IDParserTools idParserTool = new IDParserTools();

        FileReader f = new FileReader("ner_training.txt",idParserTool);


        SentenceFeatures ff = new SentenceFeatures(f);


        ff.train();

        IDSentence mySentence = new IDSentence("Ankara Milan'i çağırdı" +
                "",
        idParserTool);

        for (int i=0;i<mySentence.getWordsOfSentence().size();i++){

            List<String> features = ff.generateFeatures(mySentence,i);
            System.out.print(features +  " ");
            System.out.println(ff.predict(features) + " ");
        }


    }



}
