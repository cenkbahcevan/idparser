package NamedEntityRecognizer;

import DependecyParser.DepSentence;
import DependecyParser.DepWord;
import IDParser.IDParserTools;
import IDParser.IDSentence;
import com.sun.xml.internal.bind.v2.model.core.ID;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by cenk on 3.04.2018.
 */
public class FileReader
{

    private String fileContent = "";

    private List<IDSentence> fileLines;

    private IDParserTools idParserTool;

    private int maximumNumberOfWords = 5;

    public int getMaximumNumberOfWords() {
        return maximumNumberOfWords;
    }

    FileReader(String fileName, IDParserTools idParserTool){

        this.fileLines = new ArrayList<>();

        this.idParserTool = idParserTool;

        try {
            this.fileContent = new String(java.nio.file.Files.readAllBytes(
                    java.nio.file.Paths.get(fileName)));


            String[] fileLinesArray = this.fileContent.split("__end__of__sentence");


            List<String> myStrTestList = new ArrayList<>();

            myStrTestList.add(fileLinesArray[0]);

            for (String sentence : fileLinesArray) {

                String[] sentenceSplitted = sentence.split("\n");

                String resultSentence = "";
                List<String> resultNer = new ArrayList<>();
                for (String word : sentenceSplitted) {


                    try {
                        if (word.length()>1) {

                            resultSentence = resultSentence + word.substring(0, word.indexOf("/")) + " ";
                            resultNer.add((word.substring(word.indexOf("/"))));
                        }
                    } catch (Exception e) {
                        System.out.println("word:" + word);
                        e.printStackTrace();

                    }


                }


                    IDSentence sentence1 = new IDSentence(resultSentence, this.idParserTool);
                    if (sentence1.getWordsOfSentence().size() == resultNer.size()) {

                        if (resultNer.size()>maximumNumberOfWords){
                            this.maximumNumberOfWords = resultNer.size();
                        }

                        for (int i = 0; i < sentence1.getWordsOfSentence().size(); i++) {
                            sentence1.getWordsOfSentence().get(i).setNerTag(resultNer.get(i));

                        }
                        this.fileLines.add(sentence1);
                    } else {
                        //System.out.println(resultNer + " " + sentence1);

                    }
                }
            }

            catch (Exception e){
            e.printStackTrace();

            }

    }


    public List<IDSentence> getFileLines() {
        return fileLines;
    }


    
    public static void main(String[] args) throws Exception {



        IDParserTools myIDParserTool = new IDParserTools();

        IDSentence mySentence = new IDSentence("Aliş'i çok özledim",myIDParserTool);

        System.out.println(mySentence);

        FileReader myFileReader = new FileReader("ner_training.txt",myIDParserTool);



        System.out.println(myFileReader.getFileLines().get(0));

        System.out.println(myFileReader.getFileLines().size());

        SentenceFeatures s = new SentenceFeatures(myFileReader);

        System.out.println(s.getWeights().get("wordro"));

        


    }
}
