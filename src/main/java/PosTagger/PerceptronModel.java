package PosTagger;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Created by cenk on 22.11.2017.
 */
public class PerceptronModel {

    private Map<String, Map<String, Double>> weights_;

    private Map<String,Integer> classInt;

    private Map<String,Integer> featureInt;

    public Map<String, Integer> getClassInt() {
        return classInt;
    }

    public Map<String, Integer> getFeatureInt() {
        return featureInt;
    }

    private int epoch;

    PerceptronModel(FeatureExtractor f,
                    int epoch) {

        this.epoch = epoch;

        this.weights_ = new HashMap<String, Map<String, Double>>();

        this.classInt = new HashMap<String,Integer>();

        this.featureInt = new HashMap<String,Integer>();

        for (String className : f.getClassList()) {

            classInt.put(className, 0);
        }



        for (String featureName:f.getFeatureList()){

            this.weights_.put(featureName, new ClassWeight());

            this.featureInt.put(featureName,0);

            for (String className : f.getClassList()) {
                this.weights_.get(featureName).put(className, 0.0);
            }
        }




    }

    public String predict(List<String> features) {

        String result = "";
        double maxScore = -1;

        for (String className : this.getClassInt().keySet()) {



            double tempScoreForClass = 0;

            for (String featureName : features) {




                if (this.weights_.containsKey(featureName) && this.weights_.get(featureName).containsKey(className)) {
                        //System.out.println( className +  " "+ featureName + " " + this.weights_.get(featureName).get(className));
                    tempScoreForClass = tempScoreForClass + this.weights_.get(featureName).get(className);
                }
            }
            if (tempScoreForClass > maxScore) {
                maxScore = tempScoreForClass;
                result = className;

            }
        }


        return result;

    }

    public void train(FeatureExtractor f) {

        List<List<List<String>>> training_X = f.getTraining_features_X();

        List<List<String>> training_Y = f.getSentencesInPosTagForm();


        for (int l=0;l<training_Y.size();l++){
            for (int p=0;p<training_Y.get(l).size();p++){
                String myLabel = training_Y.get(l).get(p);
                this.classInt.put(myLabel,this.classInt.get(myLabel)+1);

            }
        }


        for (int k = 0; k < this.epoch; k++) {

            for (int i = 0; i < training_X.size(); i++) {
                List<List<String>> sentence = training_X.get(i);

                if (sentence.size() == training_Y.get(i).size()) {
                    for (int j = 0; j < sentence.size(); j++) {
                        String modelPredictionResult = predict(sentence.get(j));
                        String realResult = training_Y.get(i).get(j);

                        if (!modelPredictionResult.equals(realResult)) {

                            for (String featureName : sentence.get(j)) {


                                featureInt.put(featureName,featureInt.get(featureName) + 1);


                                double pastValueOfWrongAnswer = this.weights_.get(featureName).get(modelPredictionResult);
                                this.weights_.get(featureName).put(modelPredictionResult, pastValueOfWrongAnswer - 1);



                                double pastValueOfRealResult = this.weights_.get(featureName).get(realResult);
                                this.weights_.get(featureName).put(realResult, pastValueOfRealResult + 1);


                            }

                        }
                    }


                }

            }

        }

        //System.out.println(this.weights_.get("Ali"));

    }
}