package PosTagger;

import IDParser.IDParserTools;
import IDParser.IDSentence;

import java.lang.reflect.Array;
import java.util.*;

/**
 * Created by cenk on 15.05.2018.
 * Hidden markov model
 *
 */
public class MarkovModel {

    private Map<String,Map<String,Double>> transitionConditionalProbabilities;

    private Map<String,Map<String,Double>> emissionConditionalProbabilities;

    private Set<String> classesList;

    private Set<String> featuresList;

    private Map<String,Double> classProbabilities;

    private int totalProbabilitityNo;


    private FeatureExtractor f;


    IDParserTools idParserTool;


    public MarkovModel(IDParserTools idParserTools){

        this.idParserTool = idParserTools;

        this.f = new FeatureExtractor(
                "postag2.txt",
                idParserTools);

        f.initiateNumericalFeatureExtraction();

        this.transitionConditionalProbabilities = new HashMap<>();

        this.emissionConditionalProbabilities = new HashMap<>();

        this.classProbabilities = new HashMap<>();

        this.classesList = new HashSet<>();

        this.featuresList = new HashSet<>();

        this.f = f;

        for (String className:f.getClassList()) {



            this.transitionConditionalProbabilities.put(className,new HashMap<>());

            if (!classProbabilities.containsKey(className)){
                this.classProbabilities.put(className,1.0);
            }
            else{

                double oldValue  = this.classProbabilities.get(className);

                this.classProbabilities.put(className,oldValue + 1.0);
            }
        }

        for (String featureName : f.getFeatureList()) {

            emissionConditionalProbabilities.put(featureName,new HashMap<>());
        }



        for (String featureName:f.getFeatureList()){


            this.featuresList.add(featureName);

            for (String className : f.getClassList()) {


                emissionConditionalProbabilities.get(featureName).put(className,0.0);



            }
        }

        for (String className:classProbabilities.keySet()){
            for (String className2:classProbabilities.keySet()){
                Map<String,Double> secondMap = this.transitionConditionalProbabilities.get(className);
                secondMap.put(className2,0.0);
                this.transitionConditionalProbabilities.put(className,secondMap);
            }
        }
        System.out.println(classProbabilities);



    }


    public void forward(){

        List<List<List<String>>> training_X = f.getTraining_features_X();

        List<List<String>> training_Y = f.getSentencesInPosTagForm();


        for (int i=0;i<training_X.size();i++){

            List<List<String>> sentence = training_X.get(i);

            if (sentence.size() == training_Y.get(i).size()) {

                for (int j = 0; j < sentence.size(); j++) {

                    /*
                    Transition
                     */



                    String realResult = training_Y.get(i).get(j);

                    if (j<sentence.size()-1){

                        String nextResult = training_Y.get(i).get(j+1);
                        try {
                            double oldValue = this.transitionConditionalProbabilities.get(realResult).get(nextResult);
                            this.transitionConditionalProbabilities.get(realResult).put(nextResult, oldValue + 1);
                        }catch (Exception e){
                            //System.out.println(realResult+  "->" + nextResult);
                        }
                    }

                    List<String> sentenceFeatures = sentence.get(j);

                    for (String feature:sentenceFeatures){

                        double newValue = this.emissionConditionalProbabilities.get(feature).get(realResult) + 1;

                        this.emissionConditionalProbabilities.get(feature).put(realResult,newValue);
                    }

                    if (!classProbabilities.containsKey(realResult)){
                        this.classProbabilities.put(realResult,1.0);
                    }
                    else
                        {

                        double oldValue  = this.classProbabilities.get(realResult);

                        this.classProbabilities.put(realResult,oldValue + 1.0);


                    }

                    totalProbabilitityNo ++;
                }




            }
            }

            for (String key:this.classProbabilities.keySet()){

                double newValue = this.classProbabilities.get(key) / totalProbabilitityNo;
                this.classProbabilities.put(key,newValue);
            }

            for (String feature:this.emissionConditionalProbabilities.keySet()){
                for (String className:this.emissionConditionalProbabilities.get(feature).keySet()){
                    double newValue = this.emissionConditionalProbabilities.get(feature).get(className) / classProbabilities.get(className);
                    this.emissionConditionalProbabilities.get(feature).put(className,newValue);

                }
            }



        System.out.println(this.emissionConditionalProbabilities);

        }

        public List<String> predict(String sentence) {

            List<String> resultList = new ArrayList<>();

            IDSentence myIdSentence = new IDSentence(sentence, idParserTool);


            for (int i = 0; i < myIdSentence.getWordsOfSentence().size(); i++) {
                List<String> featuresOfCurrentWord = this.f.featurizeWord(myIdSentence, i);
                System.out.println(featuresOfCurrentWord);

                //System.out.println(featuresOfCurrentWord);
                ;


                double maximum = 1.0;
                String result = "";

                for (String className : this.classProbabilities.keySet()) {
                    double tempMax = 1.0;
                    for (String featureName : featuresOfCurrentWord) {

                        if (this.emissionConditionalProbabilities.containsKey(featureName) && this.emissionConditionalProbabilities.get(featureName).get(className) != 0) {
                            tempMax = tempMax *
                                    Math.log(this.emissionConditionalProbabilities.get(featureName).get(className));
                        }
                    }
                    if (tempMax > maximum) {
                        result = className;
                        maximum = tempMax;
                    }
                }


                resultList.add(result);
            }



            return resultList;
        }

    public void calculateAccuracy(IDParserTools idParserTool) {


        double score = 0.0;
        double allNo = 0.0;

        List<IDSentence> sentences = this.f.getGetSentencesInWordFormTest();

        List<List<String>> posTags = this.f.getSentencesinPosTagFormTest();

        for (int i = 0;
             i < sentences.size(); i++) {


            IDSentence sentence = sentences.get(i);

            List<String> wholeLabels = predict(sentence.getWholeSentence());
            List<String> posLabels = posTags.get(i);

            if (wholeLabels.size() != posLabels.size()) {
                continue;
            }

            for (int j = 0; j < wholeLabels.size(); j++) {


                allNo++;

                System.out.println(wholeLabels.get(j) + " " + posLabels.get(j));

                if (wholeLabels.get(j).equals(posLabels.get(j))) {
                    score = score + 1.0;
                }

            }

        }
        System.out.println("Score:" + score / allNo);
    }

    public double getProbabilitiy(List<String> probList,String className){
        double res = 1;

        for (String prob:probList){
            if (this.emissionConditionalProbabilities.containsKey(prob)) {
                res = res * this.emissionConditionalProbabilities.get(prob).get(className);
            }
        }
        return  res;
    }



    public List<ArrayList<String>> viterbiPredict(String sentence){

        IDSentence myIdSentence = new IDSentence(sentence, idParserTool);

        Map<String,List<String>> path = new HashMap<>();

        Map<String,Double> currentProb = new HashMap<>();

        List<String> featuresOfCurrentWord = this.f.featurizeWord(myIdSentence, 0);



        for (String className:this.classProbabilities.keySet()){
                currentProb.put(className,getProbabilitiy(featuresOfCurrentWord,className));
                path.put(className,new ArrayList<>());
        }

        for (int i=1;i<myIdSentence.getWordsOfSentence().size();i++){


            Map<String,Double> lastProb = currentProb;
            currentProb = new HashMap<>();

            for (String firstClassName:this.classProbabilities.keySet()){
                double maximumOfCurrent = 0.0;
                String maximumLabel = "";
                for (String secondClassName:this.classProbabilities.keySet()) {
                    //double currentRes = lastProb.get(secondClassName)*transitionConditionalProbabilities.get()

                    System.out.println(lastProb.get(secondClassName) +  " * " + this.transitionConditionalProbabilities.get(secondClassName).get(firstClassName) );
                    double thatRes = lastProb.get(secondClassName) *
                            this.transitionConditionalProbabilities.get(secondClassName).get(firstClassName) *
                            getProbabilitiy(featuresOfCurrentWord, firstClassName);


                    if (thatRes > maximumOfCurrent) {
                        maximumLabel = secondClassName;
                        maximumOfCurrent = thatRes;
                    }
                }


                    currentProb.put(firstClassName,maximumOfCurrent);
                    path.get(firstClassName).add(maximumLabel);

                }

                featuresOfCurrentWord = this.f.featurizeWord(myIdSentence, i);
            }



        double max_prob = -1;
        List<String> result = new ArrayList<>();

        for (String className:this.classProbabilities.keySet()){
            path.get(className).add(className);
            if (currentProb.get(className)>max_prob){
                max_prob = currentProb.get(className);
                result = path.get(className);

            }
        }

        System.out.println("Result:" + path);
        System.out.println(this.classProbabilities.keySet());
        return null;
    }


    public static void main(String[] args) throws Exception {

        IDParserTools a = new IDParserTools();

        FeatureExtractor featureExtractor = new FeatureExtractor(
                "postag2.txt",
                a);

        featureExtractor.initiateNumericalFeatureExtraction();


        MarkovModel m = new MarkovModel(a);

        m.forward();

        m.calculateAccuracy(a);




    }



}
