package PosTagger;

import IDParser.IDParserTools;
import IDParser.IDSentence;
import IDParser.IDWord;
import zemberek.morphology.analysis.tr.TurkishMorphology;
import zemberek.morphology.analysis.tr.TurkishSentenceAnalyzer;

import java.io.FileNotFoundException;
import java.util.*;

/**
 * Created by cenk on 22.11.2017.
 */
public class FeatureExtractor {



    private String separatorFromSentences = "\n";


    private String separatorBetweenSentenceAndPos = "\t";

    private String fileContent;

    //private List<List<String>> sentencesInWordForm;

    private List<IDSentence> sentencesInWordForm;

    private List<IDSentence> getSentencesInWordFormTest;

    private List<List<String>> sentencesInPosTagForm;

    private List<List<String>> sentencesinPosTagFormTest;

    private Set<String> classList;

    private List<List<List<String>>> training_features_X;

    private Set<String> featureList;

    private  List<List<List<String>>> training_Y;

    private final int[] digitArray = {0,1,2,3,4,5,6,7,8,9};

    private final char[] puncArray = {',','.',';',':','-','_','\''};

    private List<Character> puncList;

    private List<String> digitList;

    public void setSeparatorFromSentences(String separatorFromSentences) {
        this.separatorFromSentences = separatorFromSentences;
    }

    public void setSeparatorBetweenSentenceAndPos(String separatorBetweenSentenceAndPos) {
        this.separatorBetweenSentenceAndPos = separatorBetweenSentenceAndPos;
    }

    FeatureExtractor(String fileName,IDParserTools idParserTool) {





        digitList = new ArrayList<String>();

        puncList = new ArrayList<>();

        for (int no:this.digitArray){
            this.digitList.add(Integer.toString(no));
        }

        for (char c: this.puncArray){
            puncList.add(c);
        }

        training_features_X = new ArrayList<>();

        training_Y = new ArrayList<>();

        this.featureList = new HashSet<>();

        this.classList = new HashSet<>();

        sentencesInPosTagForm = new ArrayList<>();

        sentencesInWordForm = new ArrayList<>();


        sentencesinPosTagFormTest = new ArrayList<>();

        getSentencesInWordFormTest = new ArrayList<>();

        try {

            this.fileContent = new String(java.nio.file.Files.readAllBytes(
                    java.nio.file.Paths.get(fileName)));

            String[] splittedLine = this.fileContent.split(this.separatorFromSentences);

            /**
             * Training data part
             */
            for (int z=0;z<splittedLine.length-100;z++){

                String line = splittedLine[z];

                String[] currentLineSplitted = line.split(this.separatorBetweenSentenceAndPos);

                if (currentLineSplitted.length>1) {
                    String[] sentenceWords = currentLineSplitted[0].split(" ");
                    String[] posWords = currentLineSplitted[1].split(" ");

                    if (sentenceWords.length == posWords.length){

                        IDSentence currentSentence = new IDSentence(currentLineSplitted[0],idParserTool);

                        List<String> currentPosList = new ArrayList<>();

                        for (int i=0;i<sentenceWords.length;i++){

                            //currentWordList.add(sentenceWords[i]);

                            if (posWords[i].equals("_")){
                                break;
                            }

                            currentPosList.add(posWords[i]);

                        }

                        //this.sentencesInWordForm.add(currentWordList);
                        this.sentencesInPosTagForm.add(currentPosList);
                        this.sentencesInWordForm.add(currentSentence);

                        for (String pos:currentPosList){
                            this.classList.add(pos);
                        }

                    }

                }

            }

            /**
             * Test part
             */

            for (int z=splittedLine.length-100;z<splittedLine.length;z++){

                String line = splittedLine[z];

                String[] currentLineSplitted = line.split(this.separatorBetweenSentenceAndPos);

                if (currentLineSplitted.length>1) {
                    String[] sentenceWords = currentLineSplitted[0].split(" ");
                    String[] posWords = currentLineSplitted[1].split(" ");

                    if (sentenceWords.length == posWords.length){

                        IDSentence currentSentence = new IDSentence(currentLineSplitted[0],idParserTool);

                        List<String> currentPosList = new ArrayList<>();

                        for (int i=0;i<sentenceWords.length;i++){

                            //currentWordList.add(sentenceWords[i]);

                            if (posWords[i].equals("_")){
                                break;
                            }

                            currentPosList.add(posWords[i]);

                        }

                        //this.sentencesInWordForm.add(currentWordList);
                        this.sentencesinPosTagFormTest.add(currentPosList);
                        this.getSentencesInWordFormTest.add(currentSentence);



                    }

                }

            }

        }
        catch (java.nio.file.NoSuchFileException e){

            System.out.println("Yok dosya");
            System.exit(0);

        }
        catch (Exception e){
            e.printStackTrace();
        }



    }

    public boolean checkIfWordContainDigits(String word){

        for (String character:word.split("")){

            if  (this.digitList.contains(character)){
                return true;
            }

        }
        return false;
    }

    public List<String> featurizeWord(IDSentence sentence, int position){

        IDWord currentWord = sentence.getWordsOfSentence().get(position);

        String currentWordContent = sentence.getWordsOfSentence().get(position).getWordRoot();

        String currentWordRealContent = sentence.getWordsOfSentence().get(position).getWordContent();
        List<String> currentWordFeatures = new ArrayList<>();

        currentWordFeatures.add(currentWordContent);
        this.featureList.add(currentWordContent);

        if (currentWordContent.length()==1 & puncList.contains(currentWordContent.charAt(0))){
            currentWordFeatures.add("punc");
            return currentWordFeatures;
        }

        if (currentWordRealContent.substring(0, 1).equals(currentWordRealContent.substring(0, 1).toUpperCase())
                &&
                !checkIfWordContainDigits(currentWordRealContent)
                ) {
            if (currentWord.getWordIndex()!=0) {
                currentWordFeatures.add("uppercasemiddle");
            }
            else{
                currentWordFeatures.add("uppercase");
            }
        }

        if (position == 0){
            currentWordFeatures.add("inthebeginning");
        }
        if (position==sentence.getWordsOfSentence().size()-1){
            currentWordFeatures.add("intheend");
        }
        if (position!= 0 && position < sentence.getWordsOfSentence().size()-1) {
            //currentWordFeatures.add("inthemiddle");
        }
        if (currentWordRealContent.contains("'") || currentWordRealContent.contains("\"") ) {
            currentWordFeatures.add("appos");
        }
        if (checkIfWordContainDigits(currentWordContent)){
            currentWordFeatures.add("digit");
        }


        for (String candidate:currentWord.getWordPosCandidates()){
            currentWordFeatures.add(candidate);
            this.featureList.add(candidate);
        }


        int i=1;

        for(String candidate:currentWord.getWordSuffixCandidates()){
            if (!candidate.equals("nosuffix")) {
                currentWordFeatures.add("suffix" + candidate);
                this.featureList.add("suffix" + candidate);
                i++;

            }
        }

        /*
        next word part
         */

        int currentWordIndex = currentWord.getWordIndex();

        if (currentWordIndex<sentence.getWordsOfSentence().size()-2) {

            IDWord nextWord = sentence.getWordsOfSentence().get(currentWordIndex + 2);


        }

        if (currentWordIndex<sentence.getWordsOfSentence().size()-1){

            IDWord nextWord = sentence.getWordsOfSentence().get(currentWordIndex + 1);

            /*
            for (String candidate:nextWord.getWordPosCandidates()){
                currentWordFeatures.add("n1" + candidate);
                this.featureList.add("n1" + candidate);

            }
            /*
            for(String candidate:nextWord.getWordSuffixCandidates()){
                currentWordFeatures.add("n1" + candidate);
                this.featureList.add("n1" + candidate);
            }
            */


            /*
            previous word
             */
            if (currentWordIndex>0){

                IDWord beforeWord = sentence.getWordsOfSentence().get(currentWordIndex-1);



                for (String candidate:beforeWord.getWordPosCandidates().subList(0,1)){
                    if (!candidate.equals("Unknown")) {
                        currentWordFeatures.add("p1" + candidate);
                        this.featureList.add("p1" + candidate);
                    }
                }

                //currentWordFeatures.add("p1root" + beforeWord.getWordRoot());
                //this.featureList.add("p1root" + beforeWord.getWordRoot());

                /*
                for(String candidate:beforeWord.getWordSuffixCandidates()){
                    currentWordFeatures.add("p1" + candidate);
                    this.featureList.add("p1" + candidate);

                }
                */



            }



        }



        return currentWordFeatures;
    }

    public void initiateNumericalFeatureExtraction() {

        this.featureList.add("uppercasemiddle");
        this.featureList.add("uppercase");
        this.featureList.add("inthebeginning");
        this.featureList.add("inthemiddle");
        this.featureList.add("intheend");
        this.featureList.add("appos");
        this.featureList.add("digit");
        this.featureList.add("punc");

       // System.out.println(this.sentencesInWordForm.get(0));
        //System.out.println(this.sentencesInPosTagForm.get(0));
        //System.out.println(this.classList);


        for (int i = 0; i < this.sentencesInWordForm.size(); i++) {


            IDSentence currentSentence = this.sentencesInWordForm.get(i);

            List<List<String>> featuresOfWords = new ArrayList<>();

            for (int j = 0; j < currentSentence.getWordsOfSentence().size(); j++) {

                List<String> currentWordFeatures = this.featurizeWord(currentSentence,j);

               // System.out.println(currentWordFeatures);

                featuresOfWords.add(currentWordFeatures);


            }

            this.training_features_X.add(featuresOfWords);

        }
    }

    public Set<String> getClassList() {
        return classList;
    }

    public Set<String> getFeatureList() {
        return featureList;
    }


    public List<List<List<String>>> getTraining_features_X() {
        return training_features_X;
    }

    public List<List<String>> getSentencesInPosTagForm() {
        return sentencesInPosTagForm;
    }

    public List<IDSentence> getGetSentencesInWordFormTest() {
        return getSentencesInWordFormTest;
    }

    public List<List<String>> getSentencesinPosTagFormTest() {
        return sentencesinPosTagFormTest;
    }
}
