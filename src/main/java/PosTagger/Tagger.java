package PosTagger;

import IDParser.IDParserTools;
import IDParser.IDSentence;
import IDParser.IDWord;
import zemberek.morphology.analysis.WordAnalysis;
import zemberek.morphology.analysis.tr.TurkishMorphology;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by cenk on 22.11.2017.
 */

public class Tagger {

    private FeatureExtractor featureExtractor;

    private IDParserTools idParserTool;

    private TurkishMorphology tm;

    private PerceptronModel model;




    public Tagger(int epochNo,IDParserTools idParserTool) throws Exception{




        this.idParserTool = idParserTool;

        this.featureExtractor = new FeatureExtractor(
                "postag2.txt",
                idParserTool);

        featureExtractor.initiateNumericalFeatureExtraction();


        this.model= new PerceptronModel(featureExtractor,epochNo);



        model.train(featureExtractor);



    }

    public static List<String> getSuffixCandidates(List<WordAnalysis> wordAnalysis){

        List<String> result = new ArrayList<String>();
        boolean noSuffix = true;
        for (WordAnalysis analysis:wordAnalysis) {


            List<String> currentSuffixesList = analysis.suffixSurfaceList();

            //
            // (currentSuffixesList);

            for (String currentSuffix:currentSuffixesList){

                //System.out.println("currentSUffix:"  + currentSuffix + currentSuffix.equals(""));

                if (!currentSuffix.equals("")
                        && !result.contains(currentSuffix))
                {
                    noSuffix = false;
                    result.add(currentSuffix);
                }
            }
            if (noSuffix && !result.contains("nosuffix")){
                //result.add("nosuffix");
            }


        }



        System.out.println(result.size());

        return result;

    }

    public List<String> predictPos(String sentence){
        List<String> result = new ArrayList<>();

        IDSentence myIdSentence = new IDSentence(sentence,this.idParserTool);



        for (int i=0;i<myIdSentence.getWordsOfSentence().size();i++) {
            List<String> featuresOfCurrentWord = this.featureExtractor.featurizeWord(myIdSentence, i);
            //System.out.println(featuresOfCurrentWord);

            //System.out.println(featuresOfCurrentWord);

            result.add(this.model.predict(featuresOfCurrentWord));
        }
        return result;

    }

    public List<String> predictPos(IDSentence myIdSentence){

        List<String> result = new ArrayList<>();

        for (int i=0;i<myIdSentence.getWordsOfSentence().size();i++) {
            List<String> featuresOfCurrentWord = this.featureExtractor.featurizeWord(myIdSentence, i);
            //System.out.println(featuresOfCurrentWord);

            //System.out.println(featuresOfCurrentWord);

            result.add(this.model.predict(featuresOfCurrentWord));
        }
        return result;
    }

    public void calculateAccuracy(){



        double score = 0.0;
        double allNo = 0.0;

        List<IDSentence> sentences = this.featureExtractor.getGetSentencesInWordFormTest();

        List<List<String>> posTags = this.featureExtractor.getSentencesinPosTagFormTest();

        for (int i=0;
             i<sentences.size();i++){



            IDSentence sentence = sentences.get(i);

            List<String> wholeLabels = predictPos(sentence);
            List<String> posLabels = posTags.get(i);

            if (wholeLabels.size() != posLabels.size()){
                continue;
            }

            for (int j=0;j<wholeLabels.size();j++){


                allNo++;

                System.out.println(wholeLabels.get(j) + " " + posLabels.get(j));

                if (wholeLabels.get(j).equals(posLabels.get(j))){
                    score = score + 1.0;
                }

            }

        }
        System.out.println("Score:" + score/allNo   );
    }


    public static void main(String[] args) throws Exception {
//        PerceptronModel Y = new PerceptronModel(X,10);

  //      Y.train();

    //    System.out.println(X.checkIfWordContainDigits("Cenk1"));

        IDParserTools myParserTool = new IDParserTools();

       Tagger myTagger = new Tagger(5,myParserTool);



        //System.out.println(myTagger.featureExtractor.getGetSentencesInWordFormTest().get(0));

        myTagger.calculateAccuracy();

        System.out.println(myTagger.predictPos("Filmi Erdoğan izledi"));


        // System.out.println(myTagger.model.getClassInt());

        //System.out.println(myTagger.featureExtractor.getClassList());
    }

}
