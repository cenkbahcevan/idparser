package DependecyParser;

import IDParser.IDParserTools;
import PosTagger.MarkovModel;
import PosTagger.Tagger;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Created by cenk on 22.12.2017.
 * Oracleları yap
 */
public class Parser {


    private Tagger tagger;

    private MarkovModel markovTagger;


     Map<String,Map<String,Double>> weights_;

    private List<String> relations;

    private List<DepWord> buffer;

    private List<DepWord> stack;

    private List<List<String>> training_features_X;

    private List<String> training_classes_Y;


    public  Parser(IDParserTools idParserTools) throws Exception {

        tagger = new Tagger(5,idParserTools);

        markovTagger = new MarkovModel(idParserTools);

        markovTagger.forward();

        training_features_X = new ArrayList<>();

        training_classes_Y = new ArrayList<>();



        weights_ = new HashMap<>();

    }

    public Tagger getTagger() {
        return tagger;
    }

    public List<String> featurizeWord(){

        List<String> currentFeatures = new ArrayList<>();
        if (this.stack.size()<2){
            currentFeatures.add("lowerthan2autoshift");
            return currentFeatures;
        }
        else{
            DepWord currentWord = this.stack.get(this.stack.size()-1);
            DepWord currentWord2 = this.stack.get(this.stack.size()-2);
            //currentFeatures.add("nextpos" + currentWord.getPosTag());
            //currentFeatures.add("currentword" + currentWord.getWordRoot() + "nextword");
            //currentFeatures.add("currentpos" + currentWord2.getPosTag());
            currentFeatures.add("nextpos" + currentWord.getPosTag() + "currentpos" + currentWord2.getPosTag());



            if (this.buffer.size()>0){
                currentFeatures.add("bufferpos" + this.buffer.get(0).getPosTag() + "nextpos" + currentWord.getPosTag());
            }





        }

        return currentFeatures;
    }

    public String predict(List<String> features){
        String maxLabel = "";
        double maxScore = -1;

        for (String className:this.weights_.keySet()){
            double tempScore = 0.0;
            for (String feature:features){
                if(this.weights_.get(className).containsKey(feature)){
                        tempScore = tempScore + this.weights_.get(className).get(feature);
                }
            }

            if (tempScore > maxScore){
                maxScore = tempScore;
                maxLabel = className;
            }

        }

        return maxLabel;
    }


    public String checkRelation(DepWord d1,DepWord d2){

        //System.out.println(d1);
        //System.out.println(d2);

        //System.out.println();
        if (d2.getWordRelated() == d1.getWordNo()){
            return "LEFTARC";
        }
        else if (d1.getWordRelated() == d2.getWordNo()){
            return "RIGHTARC";
        }
        return "SHIFT";
    }

    public String getLabel(){

        //System.out.println(this.stack);

        if (this.stack.size()<2){
           return "SHIFT";
        }

        else{
            //System.out.println(this.stack);
            DepWord currentWord = this.stack.get(this.stack.size()-1);
            DepWord currentWord2 = this.stack.get(this.stack.size()-2);

            //System.out.println("else icindeyim" + currentWord + " " + currentWord2);


            return checkRelation(currentWord,currentWord2);

        }


    }

    public void addFeaturesToClass(String className,List<String> featuresList){
        if (this.weights_.containsKey(className)){

            for (String feature:featuresList){
                if (this.weights_.get(className).containsKey(feature)){
                    double oldValue = this.weights_.get(className).get(feature);
                    this.weights_.get(className).put(feature,oldValue + 1.0);
                }
                else{
                    this.weights_.get(className).put(feature,1.0);
                }
            }

        }

        else{
                weights_.put(className,new HashMap<>());
        }
    }


    public void parse(DepSentence thatSentence){

        this.buffer = new ArrayList<DepWord>();

        this.stack = new ArrayList<DepWord>();

        int loopSize = 0;

        for (DepWord word:thatSentence.getWords()){
            this.buffer.add(word);
        }


        while ((this.buffer.size()>0 || this.stack.size()>1) && loopSize < 50){

            List<String> currentFeatures = this.featurizeWord();
            String currentLabel = this.getLabel();





            //System.out.println("currentLabel:" +  currentLabel);
            training_features_X.add(currentFeatures);
            training_classes_Y.add(currentLabel);
            //System.out.println(currentFeatures + " " + currentLabel);

            this.addFeaturesToClass(currentLabel,currentFeatures);


            if (currentLabel.equals("SHIFT")){
                if (this.buffer.size()>0) {
                    this.stack.add(this.buffer.get(0));
                    this.buffer.remove(0);
                }
                else{
                    break;
                }
            }

            else if (currentLabel.equals("LEFTARC")){
                DepWord currentWord = this.stack.get(this.stack.size()-1);
                DepWord currentWord2 = this.stack.get(this.stack.size()-2);
                //System.out.printf("Relation between %s %s \n",currentWord,currentWord2);
                this.stack.remove(this.stack.size()-2);


            }
            else if (currentLabel.equals("RIGHTARC")){
                DepWord currentWord = this.stack.get(this.stack.size()-1);
                DepWord currentWord2 = this.stack.get(this.stack.size()-2);
                //System.out.printf("Relation between %s %s\n",currentWord,currentWord2);

                this.stack.remove(this.stack.size()-1);

            }



            //System.out.println(this.stack);
            loopSize++;


        }



    }

    public Map<String, Map<String, Double>> getWeights_() {
        return weights_;
    }

    public Map<Integer,List<Integer>> parseModel(DepSentence thatSentence){

        Map<Integer,List<Integer>> relationMap = new HashMap<>();


        int loopNo = 0;



        this.buffer = new ArrayList<DepWord>();

        this.stack = new ArrayList<DepWord>();

        int i=0;

        for (DepWord word:thatSentence.getWords()){
            this.buffer.add(word);

            relationMap.put(i+1,new ArrayList<>());
            i++;
        }

        while ((this.buffer.size()>0 || this.stack.size()>3) && loopNo < 70){

            List<String> currentFeatures = this.featurizeWord();
            String currentLabel = this.predict(currentFeatures);
            //System.out.println("currentLabel:" + currentLabel + " currentFeatures:" + currentFeatures);

            if (currentLabel.equals("SHIFT")){

                if (this.buffer.size()>0) {
                    this.stack.add(this.buffer.get(0));
                    this.buffer.remove(0);
                }
                else{
                    break;
                }
            }

            else if (currentLabel.equals("LEFTARC")){
                DepWord currentWord = this.stack.get(this.stack.size()-1);
                DepWord currentWord2 = this.stack.get(this.stack.size()-2);
                //System.out.printf("Relation between %s %s \n",currentWord,currentWord2);
                relationMap.get(currentWord.getWordNo()).add(currentWord2.getWordNo());
                this.stack.remove(this.stack.size()-2);


            }
            else if (currentLabel.equals("RIGHTARC")){
                DepWord currentWord = this.stack.get(this.stack.size()-1);
                DepWord currentWord2 = this.stack.get(this.stack.size()-2);
                //System.out.printf("Relation between %s %s\n",currentWord,currentWord2);
                relationMap.get(currentWord2.getWordNo()).add(currentWord.getWordNo());
                this.stack.remove(this.stack.size()-1);

            }

            //System.out.println(this.stack);
            loopNo++;
        }
        for (DepWord stackWord:this.stack){
            if (relationMap.size() != stackWord.getWordNo()) {
                relationMap.get(relationMap.size()).add(stackWord.getWordNo());
            }
        }

        int loopNo2 = 0;

        while(this.stack.size()>2 && loopNo2 < 5){

            List<String> currentFeatures = this.featurizeWord();
            String currentLabel = this.predict(currentFeatures);

            if (currentLabel.equals("LEFTARC")){
                DepWord currentWord = this.stack.get(this.stack.size()-1);
                DepWord currentWord2 = this.stack.get(this.stack.size()-2);
                //System.out.printf("Relation between %s %s \n",currentWord,currentWord2);
                relationMap.get(currentWord.getWordNo()).add(currentWord2.getWordNo());
                this.stack.remove(this.stack.size()-2);


            }
            else if (currentLabel.equals("RIGHTARC")){
                DepWord currentWord = this.stack.get(this.stack.size()-1);
                DepWord currentWord2 = this.stack.get(this.stack.size()-2);
                //System.out.printf("Relation between %s %s\n",currentWord,currentWord2);
                relationMap.get(currentWord2.getWordNo()).add(currentWord.getWordNo());


                this.stack.remove(this.stack.size()-1);

            }

            loopNo2++;

        }

        System.out.println(relationMap);
        return relationMap;

    }

    public DepSentence makeDepSentenceFromRawText(String text){

        DepSentence resultSentence = new DepSentence();

        List<String> posOfWords = this.getTagger().predictPos(text);

        String[] sentenceSplitted = text.split(" ");

        for (int i=0;i<sentenceSplitted.length;i++ ){
            DepWord currentWord = new DepWord(sentenceSplitted[i],posOfWords.get(i),0);
            currentWord.setWordNo(i+1);
            resultSentence.addWord(currentWord);

        }
        return resultSentence;
    }


}
