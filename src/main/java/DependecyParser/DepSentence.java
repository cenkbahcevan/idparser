package DependecyParser;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by cenk on 22.12.2017.
 */
public class DepSentence {


    private List<DepWord> words;


    public DepSentence() {
        this.words = new ArrayList<>();
    }

    void addWord(DepWord word){

        this.words.add(word);
    }

    public List<DepWord> getWords() {
        return words;
    }

    @Override
    public String toString() {
        return "DepSentence{" +
                "words=" + words +
                '}';
    }
}
