package DependecyParser;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import PosTagger.*;

/**
 * Created by cenk on 22.12.2017.
 */
public class ConllPreparer {


    public static DepSentence constructSentence(String sentence) {

        DepSentence mySentence = new DepSentence();

        String[] sentenceSplitted = sentence.split("\n");

        for (int i = 0; i < sentenceSplitted.length; i++) {

        }

        return null;
    }

    private Map<Integer, Integer> relations;

    private String fileContent;

    private String[] sentencesOfCONLL;



    List<DepSentence> depSentences;

    public List<DepSentence> getDepSentences() {
        return depSentences;
    }


    public ConllPreparer(String fileName,int a){

        depSentences = new ArrayList<>();

        try {
            this.fileContent = new String(java.nio.file.Files.readAllBytes(
                    java.nio.file.Paths.get(fileName)));


            String[] linesSplitted = this.fileContent.split("\n");

            DepSentence myDepSentence = new DepSentence();

            for (String sentence : linesSplitted) {


                    if (sentence.trim().equals("")){

                        depSentences.add(myDepSentence);
                        myDepSentence = new DepSentence();
                    }
                    else{

                        DepWord currentWord = new DepWord(sentence);

                        myDepSentence.addWord(currentWord);
                    }
                }




        } catch (Exception e) {
            e.printStackTrace();
        }


    }



    public ConllPreparer(String fileName) {


        depSentences = new ArrayList<>();

        try {
            this.fileContent = new String(java.nio.file.Files.readAllBytes(
                    java.nio.file.Paths.get(fileName)));


            sentencesOfCONLL = this.fileContent.split("\n\n");


            for (String sentence : sentencesOfCONLL) {
                DepSentence mySentence = new DepSentence();
                String[] sentence_splitted_to_words = sentence.split("\n");
                DepSentence myDepSentence = new DepSentence();

                if (sentence_splitted_to_words.length > 2) {


                    for (int i = 2; i < sentence_splitted_to_words.length; i++) {


                        String[] wordLineSplitted = sentence_splitted_to_words[i].split("\t");

                        if (wordLineSplitted.length > 7) {

                            String wordNo = wordLineSplitted[0];
                            if (wordNo.contains("-")) {
                                continue;
                            }

                            int myWordNo = Integer.parseInt(wordLineSplitted[0]);

                            String wordContent = wordLineSplitted[2];
                            String wordPos = wordLineSplitted[4];

                            String wordRelated = wordLineSplitted[6];


                            int wordRelatedInt = -1;
                            try {
                                wordRelatedInt = Integer.parseInt(wordRelated);

                            } catch (Exception e) {

                            }
                            DepWord myWord = new DepWord(wordContent, wordPos, wordRelatedInt);
                            myWord.setWordNo(myWordNo);


                            myDepSentence.addWord(myWord);


                        }

                    }
                    this.depSentences.add(myDepSentence);


                }
            }

            System.out.println(this.depSentences.size());

        } catch (Exception e) {
            e.printStackTrace();
        }


    }

    public String getFileContent() {
        return fileContent;
    }

}