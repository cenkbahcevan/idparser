package DependecyParser;

/**
 * Created by cenk on 22.12.2017.
 */
public class DepWord {

    private String posTag;

    private String wordContent;

    private String wordRoot;

    private int wordNo;

    private int wordRelated;


    public String getWordRoot() {
        return wordRoot;
    }

    public String getWordContent() {
        return wordContent;
    }

    public int getWordRelated() {
        return wordRelated;
    }

    public void setWordRelated(int wordRelated) {
        this.wordRelated = wordRelated;
    }

    public void setWordNo(int wordNo) {
        this.wordNo = wordNo;
    }

    public DepWord(String line){


            String[] lineSplitted = line.split("\t");

            if (lineSplitted.length>4){

                this.wordNo = Integer.parseInt(lineSplitted[0]);

                this.wordRoot = lineSplitted[2];

                this.wordContent = lineSplitted[1];

                this.posTag = lineSplitted[3];

                //wordPos2 = lineSplitted[4];
            }

        }

    public DepWord(String posTag, String wordContent) {
        this.posTag = posTag;
        this.wordContent = wordContent;

    }

    public DepWord(String wordContent, String posTag, int wordRelated) {
        this.posTag = posTag;
        this.wordContent = wordContent;
        this.wordRelated = wordRelated;
    }

    @Override
    public String toString() {
        return "DepWord{" + this.wordNo + " " + this.wordContent + " " + this.posTag + " " + this.wordRelated + "}";

    }
    public String getPosTag() {
        return posTag;
    }

    public int getWordNo() {
        return wordNo;
    }
}
