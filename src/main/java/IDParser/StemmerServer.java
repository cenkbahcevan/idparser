package IDParser;

/**
 * Created by cenk on 8.12.2017.
 */

import java.io.IOException;
import java.net.InetSocketAddress;
import java.nio.ByteBuffer;
import java.nio.channels.SelectionKey;
import java.nio.channels.Selector;
import java.nio.channels.ServerSocketChannel;
import java.nio.channels.SocketChannel;
import java.nio.charset.Charset;
import java.util.Iterator;
import java.util.Set;

import IDParser.IDSentence;
import IDParser.SymptomExtracter;

import java.io.IOException;
import java.net.InetSocketAddress;
import java.nio.ByteBuffer;
import java.nio.channels.SelectionKey;
import java.nio.channels.Selector;
import java.nio.channels.ServerSocketChannel;
import java.nio.channels.SocketChannel;
import java.util.Iterator;
import java.util.Set;

/**
 * Created by cenk on 11.07.2017.
 */
public class StemmerServer extends Thread {

    private Selector selector;

    private String adress;

    private int portNo;


    private ServerSocketChannel crunchifySocket;

    private InetSocketAddress crunchifyAddr;

    private int ops;

    private SelectionKey selectKy;


    private SymptomExtracter serverSymptomExtractor;

    IDParserTools idParserTool;


    StemmerServer(String adress, int portNo,IDParserTools idParserTool) throws Exception


    {


        this.idParserTool = idParserTool;


        this.selector = Selector.open(); // selector is open here


        this.crunchifySocket = ServerSocketChannel.open();
        this.crunchifyAddr = new InetSocketAddress(adress, portNo);



        crunchifySocket.bind(crunchifyAddr);

        crunchifySocket.configureBlocking(false);

        int ops = crunchifySocket.validOps();

        this.selectKy = crunchifySocket.register(selector, ops, null);

        this.serverSymptomExtractor = new SymptomExtracter(this.idParserTool);



    }

    public void analyzeQuery() throws IOException {

        System.out.println("i'm a stemmer server and waiting....");
        // Selects a set of keys whose corresponding channels are ready for I/O operations
        selector.select();

        // token representing the registration of a SelectableChannel with a Selector
        Set<SelectionKey> crunchifyKeys = selector.selectedKeys();
        Iterator<SelectionKey> crunchifyIterator = crunchifyKeys.iterator();

        while (crunchifyIterator.hasNext()) {
            SelectionKey myKey = crunchifyIterator.next();

            // Tests whether this key's channel is ready to accept a new socket connection
            if (myKey.isAcceptable()) {
                SocketChannel crunchifyClient = crunchifySocket.accept();

                crunchifyClient.socket().setKeepAlive(true);


                // Adjusts this channel's blocking mode to false
                crunchifyClient.configureBlocking(false);

                // Operation-set bit for read operations
                crunchifyClient.register(selector, SelectionKey.OP_READ);
                System.out.println("Connection Accepted: " + crunchifyClient.getLocalAddress() + "\n");

                // Tests whether this key's channel is ready for reading
            } else if (myKey.isReadable()) {

                SocketChannel crunchifyClient = (SocketChannel) myKey.channel();




                crunchifyClient.socket().setKeepAlive(true);

                ByteBuffer crunchifyBuffer = ByteBuffer.allocate(256);

                if (crunchifyClient.read(crunchifyBuffer)==-1){
                    crunchifyClient.close();
                    return;
                }

                crunchifyClient.read(crunchifyBuffer);


                String result = new String(crunchifyBuffer.array(),"UTF-8").trim();

                System.out.println("Message received: " + result);



                IDSentence thisSentence = new IDSentence(result,idParserTool);


                String outputResult = thisSentence.toRootFormSentence()+"\n";


                ByteBuffer output = Charset.forName("UTF-8").encode(outputResult);

                
                crunchifyClient.write(output);

            }
            crunchifyIterator.remove();
        }
    }

    public void run(){
        try {

            while (true) {
                this.analyzeQuery();


            }
        }
        catch (Exception e){
            e.printStackTrace();
            System.out.println("error");
            try {
                this.crunchifySocket.close();




            } catch (IOException e1) {

                e1.printStackTrace();
            }
        }
    }



}
