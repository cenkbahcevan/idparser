package IDParser;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;


/**
 * Created by cenk on 20.09.2017.
 */
public class Constants {

    final String[] bodyParts = {
            "göz",
            "kol",
            "bacak",
            "akciğer",
            "karaciğer",
            "kalp",
            "beyin",
            "bağırsak",
            "parmak",
            "ayak",
            "kulak",
            "sırt",
            "kalça",
            "penis",
            "vajina",
            "haya",
            "kaş",
            "baş",
            "karn",
            "boğaz",
            "bilek",
            "el",
            "elim",
            "burn",
            "deri",
            "diş",
            "mide",
            "akrep",
            "böcek",
            "örümcek",
            "göğüs",
            "ses",
            "meme",
            "sik",
            "kas",
            "nefes",
            "bel",
            "eklem",
            "böbrek",
            "saç",
            "kaburga",
            "diz",
            "erken",
            "geç",
            "boyn",
            "omuz",
            "idrar",
            "geniz",
            "çiş",
            "dışkı",
            "testis",
            "kasığ",
            "kasık",
            "makat",
            "tırnak",
            "ense",
            "dudak",
            "baldır",
            "yüz",
            "dil",
            "ağız"
    };


    


    final String[] affectedParts = {
            "ağrı",
            "kan",
            "şiş",
            "ağrı",
            "kanama",
            "kanamak",
            "burk",
            "kır",
            "yan",
            "leke",
            "kana",
            "ağr",
            "morar",
            "kızar",
            "morar",
            "çatlak",
            "yara",
            "tık",
            "sararma",
            "ak",
            "sok",
            "ısır",
            "kaşıntı",
            "yaşar",
            "kıs",
            "dar",
            "bulantı",
            "çınl",
            "dök",
            "uyuş",
            "dön",
            "taş",
            "boşal",
            "tutul",
            "ac",
            "uyuş",
            "sıkış",
            "acı",
            "karx",
            "sancı"

    };


    final String[] generalAffects = {
            "ateş",
            "hal",
            "üş",
            "ishal",
            "bayıl",
            "sert",
            "çarpıntı",
            "öksür",
            "titr",
            "kabız",
            "karıncalan",
            "kus",
            "yutkun",
            "terle",
            "horla",
            "hıçkırık",

    };

    final String[] multiBodyArray = {
            "diş et",
            "ayak baş parmağ",
            "ayak parmağ baş",
            "ayak parmağ",
            "büyük abdest",
            "koltuk alt",
            "diş et",
            "cinsel organ"
    };

    final String[] thankYouArray = {
            "teşekkürler",
            "tessekur",
            "tesekkur",
            "sağol",
            "sağolun",
            "saol",
            "saolun",
            "teşekkür",
            "teşekürler",
            "eyvallah",
            "tesekkurler",
            "tsk",
            "tskr",
            "tskrlr",
            "tesk",
            "aferin",
            "tşkler",
            "tşk",
            "sağolun",
            "rica"
    };

    final String[] greetingsArray = {
            "merhaba",
            "selam",
            "selamlar",
            "selamunaleyküm",
            "sa",
            "selamun aleykum",
            "selamun",
            "merhabalar",
            "nasılsın",
            "hey",
            "naber",
            "nabersin",
            "nasıl gidiyor",
            "napıyorsun",
            "günaydın",
            "yakşamlar",
            "help",
            "slm",
            "mrb",
            "mrhb",
            "mrhba",
            "bb"
    };

    final String[] timeValues = {
            "haf",
            "gü",
            "ay",
            "saat",
            "yarın",
            "pazartesi",
            "salı",
            "çarşamba",
            "perşembe",
            "cu",
            "cumartesi",
            "pazar",
            "ocak",
            "şubat",
            "mart",
            "nisan",
            "mayıs",
            "haziran",
            "temmuz",
            "ağustos",
            "sabah",
            "akşam",
            "öğlen"
    };

    final String[] numericValues = {
            "bir",
            "iki",
            "üç",
            "dört",
            "beş",
            "altı",
            "yedi",
            "sekiz",
            "dokuz",
            "on",
    };

    final String[] understandValues = {
            "anladım",
            "tamam",
            "tamamdır",
            "ok",
            "okey",
            "okay",
            "peki"
    };

    final String[] farewellValues = {
            "görüşürüz",
            "akşamlar",
            "bye",
            "bb"
    };

    final String[] appointmentValues = {
            "randevu",
            "rezarvasyon"
    };

    final String[] hospitalEntities = {
            "şube",
            "hastane",
            "acıbadem"
    };

    final String[] tedaviEntities = {
            "checkup",
            "check up",
            "ultrason",
            "göz muayene",



    };

    final String[] ucretEntities = {
            "göz",
            "kulak",
            "burun",
            "boğaz",
            "check",
            "doğum",
            "çocuk",
            "diyet",
            "mr"
    };

    final String[] negativeAffects = {
            "hisset"
    };




    private List<String> bodyPartsListForm;

    private List<String> affectsListForm;

    private List<String> multiBodyParts;

    private List<String> generalAffectsListForm;

    private List<String> thankYouMessages;

    private List<String> greetingsList;

    private List<String> timeValuesList;

    private List<String> understandValuesList;

    private List<String> farewellValuesList;

    private List<String> appointmentValuesList;

    private List<String> priceValuesList;

    private List<String> numericList;

    private List<String> negativeAffectsList;


    Constants(){

        this.bodyPartsListForm = new ArrayList<String>();

        this.affectsListForm = new ArrayList<String>();

        this.generalAffectsListForm = new ArrayList<String>();

        this.multiBodyParts = new ArrayList<>();

        this.thankYouMessages  = new ArrayList<>();

        this.greetingsList = new ArrayList<String>();

        this.timeValuesList = new ArrayList<String>();

        this.understandValuesList = new ArrayList<String>();

        this.farewellValuesList = new ArrayList<String >();

        this.appointmentValuesList = new ArrayList<>();

        this.priceValuesList = new ArrayList<>();

        this.numericList = new ArrayList<>();

        this.negativeAffectsList = new ArrayList<>();

        for (String word:bodyParts){

            bodyPartsListForm.add(word);

        }

        for (String word:affectedParts){

            affectsListForm.add(word);

        }

        for (String word:generalAffects){
            generalAffectsListForm.add(word);
        }

        for (String word:multiBodyArray){
            this.multiBodyParts.add(word);
        }

        for (String word:thankYouArray){
            this.thankYouMessages.add(word);
        }

        for (String word:greetingsArray){
            this.greetingsList.add(word);
        }

        for (String value:timeValues){
            this.timeValuesList.add(value);
        }

        for (String word:understandValues){
            this.understandValuesList.add(word);
        }

        for (String word:farewellValues){
            this.farewellValuesList.add(word);
        }

        for (String word:appointmentValues){
            this.appointmentValuesList.add(word);
        }

        for (String word:ucretEntities){
            this.priceValuesList.add(word);
        }

        for (String word:numericValues){
            this.numericList.add(word);

        }

        for (String word:negativeAffects){
            this.negativeAffectsList.add(word);
        }

        for (int i=1;i<=31;i++){
            this.numericList.add("" + i);
        }


    }

    public boolean numericContains(String word){

        return numericList.contains(word);
    }


    public boolean organContains(String word){

        return bodyPartsListForm.contains(word);
    }

    public boolean affectContains(String word){

        return affectsListForm.contains(word);
    }

    public boolean generalAffectsContains(String word){

        return generalAffectsListForm.contains(word);
    }

    public boolean multiBodyContains(String word){
        return multiBodyParts.contains(word);
    }

    public  boolean thankContains(String word){
        return thankYouMessages.contains(word);
    }

    public boolean greetingContains(String word){
        return this.greetingsList.contains(word);
    }


    public boolean timeContains(String word){
        return this.timeValuesList.contains(word);
    }

    public boolean understandContains(String word)
    {
        return this.understandValuesList.contains(word);
    }

    public boolean farewellContains(String word){
        return this.farewellValuesList.contains(word);
    }

    public boolean appointmentContains(String word){
        return this.appointmentValuesList.contains(word);
    }

    public boolean ucretContains(String word){
        return this.priceValuesList.contains(word);
    }

    public boolean negativeAffectContains(String word){
        return this.negativeAffectsList.contains(word);
    }

}
