package IDParser;

/**
 * Created by cenk on 28.03.2018.
 */

import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.*;

/**
 * Created by cenk on 25.06.2017.
 */
public class BayesianOccurenceClassifier  {


    private int textFeatureNo=0;

    private int labelsNo=1;

    private int dataStartNo;

    private Map<String,Double> classesAndProbabilities;

    private Map<String,Integer> wordNumberForClasses;


    private Map<String,Map<String,Double>> wordProbabilityInClasses;

    private Set<String> classifierVocabulary;


    double totalDocumentNo=0.0;

    private ContentPreparer classifiersContent;



    BayesianOccurenceClassifier(ContentPreparer contentPreparer){


        this.classesAndProbabilities = new HashMap<String,Double>();

        this.wordProbabilityInClasses = new HashMap<String,Map<String,Double>>();

        this.classifiersContent =  contentPreparer;

        this.classifierVocabulary = new HashSet<String>();

        this.wordNumberForClasses  = new HashMap<String,Integer>();

        if (classifiersContent.isFirstLineColumnNames()){

            this.dataStartNo  = 1;
            this.totalDocumentNo  = classifiersContent.getSentences().size()-1;

        }
        else{
            this.dataStartNo = 0;
            this.totalDocumentNo = classifiersContent.getSentences().size();

        }

    }

    public void calculatePriorProbabilities(){




        List<String> myLabels = classifiersContent.getLabels();

        for (String tempClass:myLabels){



            if (this.classesAndProbabilities.containsKey(tempClass)){

                double newValue  = this.classesAndProbabilities.get(tempClass)+1;

                this.classesAndProbabilities.put(tempClass,newValue);
            }

            else{

                this.classesAndProbabilities.put(tempClass,1.0);
            }


        }

        for (String key:this.classesAndProbabilities.keySet()){

            double newValue  = this.classesAndProbabilities.get(key) / this.totalDocumentNo;

            this.classesAndProbabilities.put(key,newValue);



            //inefficiency den kacmak icin
            this.wordProbabilityInClasses.put(key,new HashMap<String,Double>());

            this.wordNumberForClasses.put(key,0);

        }

        System.out.println(classesAndProbabilities);


    }

    public void calculateConditionalProbabilities(){


        List<IDSentence> mySentences = this.classifiersContent.getSentences();

        List<String> myLabels = this.classifiersContent.getLabels();

        for (int i=0;i<mySentences.size();i++){


            String templabel = myLabels.get(i);

            IDSentence currentSentence  = mySentences.get(i);




            if (!this.wordNumberForClasses.containsKey(templabel)) {
                this.wordNumberForClasses.put(templabel, currentSentence.getWordsOfSentence().size());
            }
            else{

                int newValue  = this.wordNumberForClasses.get(templabel)+currentSentence.getWordsOfSentence().size();

                this.wordNumberForClasses.put(templabel, newValue);

            }


            for (IDWord word:currentSentence.getWordsOfSentence()){

                String normalizedWord = word.getWordRoot();

                this.classifierVocabulary.add(normalizedWord);

                if (wordProbabilityInClasses.get(templabel).containsKey(normalizedWord)){

                    double nextValue  = wordProbabilityInClasses.get(templabel).get(normalizedWord)+1;

                    wordProbabilityInClasses.get(templabel).put(normalizedWord,nextValue);


                }

                else{
                    wordProbabilityInClasses.get(templabel).put(normalizedWord,1.0);

                }


            }

        }

        for (String key:wordProbabilityInClasses.keySet()){
            for (String word:wordProbabilityInClasses.get(key).keySet()){


                double newValue = (wordProbabilityInClasses.get(key).get(word) + 1)
                        /
                        (this.classifierVocabulary.size() + this.wordNumberForClasses.get(key));


                wordProbabilityInClasses.get(key).put(word,newValue);


            }
        }




        //System.out.println(wordProbabilityInClasses);



    }

    public void train(){

        this.calculatePriorProbabilities();

        this.calculateConditionalProbabilities();

        //System.out.println(this.wordProbabilityInClasses);


    }

    public String predict(IDSentence sentence){

        String trueClass="";

        double tempScore = 0;


        for (String key:wordProbabilityInClasses.keySet()){


            double classTempScore = 1;

            for (IDWord word:sentence.getWordsOfSentence()){

                if (wordProbabilityInClasses.get(key).containsKey(word.getWordRoot())) {
                    //System.out.println("feature:" + key);
                    classTempScore = classTempScore * wordProbabilityInClasses.get(key).get(word.getWordRoot());
                }
                else{
                    classTempScore = classTempScore * 0.001;
                }

            }

            if (classTempScore>tempScore){
                trueClass = key;
                tempScore = classTempScore;
            }



        }

        return trueClass;
    }


    public void setTextFeatureNo(int textFeatureNo) {
        this.textFeatureNo = textFeatureNo;
    }

    public void setLabelsNo(int labelsNo) {
        this.labelsNo = labelsNo;
    }

    public static void main(String[] args) throws Exception {
        IDParserTools idParserTool = new IDParserTools();

        FileReader myReader = new FileReader("training.csv");


        ContentPreparer myPreparer = new ContentPreparer(myReader.getFileContent(),
                                                        "\n",",", true,idParserTool);

        BayesianOccurenceClassifier bayesianOccurenceClassifier = new BayesianOccurenceClassifier(myPreparer);

        bayesianOccurenceClassifier.train();

        IDSentence mySentence = new IDSentence("ameliyat ücreti ne kadar",idParserTool);
        System.out.println(bayesianOccurenceClassifier.predict(mySentence));





    }

}
