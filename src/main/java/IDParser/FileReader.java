package IDParser;

import sun.misc.IOUtils;

import java.io.*;
import java.nio.file.Files;
import java.util.Scanner;

/**
 * Created by cenk on 24.06.2017.
 */
public class FileReader {

    private String fileName;

    private String fileContent;





    public boolean checkIfFileExists() {

        File f = new File(this.fileName);

        return f.isFile();
    }


    public String getFileName() {
        return fileName;
    }

    public String getFileContent() {
        return fileContent;
    }


    FileReader(String fileName) throws FileNotFoundException {

        this.fileName = fileName;


        try {

            this.fileContent = new String(java.nio.file.Files.readAllBytes(
                    java.nio.file.Paths.get(this.fileName)),"UTF-8");

        }
        catch (java.nio.file.NoSuchFileException e){

            System.out.println("Yok dosya");
            System.exit(0);

        }

        catch (Exception e){
            e.printStackTrace();
        }




    }




}
