package IDParser;

import IDParser.IDWord;
import zemberek.morphology.ambiguity.Z3MarkovModelDisambiguator;
import zemberek.morphology.analysis.SentenceAnalysis;
import zemberek.morphology.analysis.WordAnalysis;
import zemberek.morphology.analysis.tr.TurkishMorphology;
import zemberek.morphology.analysis.tr.TurkishSentenceAnalyzer;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

/**
 * Created by cenk on 28.05.2017.
 */
public class IDSentence implements Iterable {



    private String labelClassified;

    public String getLabelClassified() {
        return labelClassified;
    }

    public void setLabelClassified(String labelClassified) {
        this.labelClassified = labelClassified;
    }

    private List<IDWord> wordsOfSentence;

    private List<String> timeValuesOfSentence;

    String wholeSentence;

    Constants C;

    //private Z3MarkovModelDisambiguator disambiguator;

    //TurkishSentenceAnalyzer analyzer;


    public List<IDWord> getWordsOfSentence() {
        return wordsOfSentence;
    }

    public void setWordsOfSentence(List<IDWord> wordsOfSentence) {
        this.wordsOfSentence = wordsOfSentence;
    }

    public String getWholeSentence(){
        return this.wholeSentence;
    }

    private List<String> subSentences;


    public String toSentenceString(){

        String result = "";

        for (IDWord word:this.wordsOfSentence){

            result  =result + word.getWordRoot() +  " ";
        }

        return result.substring(0,result.length()-1);

    }



    @Override
    public String toString() {
        String result = "";

        for (IDWord word:this.wordsOfSentence){

           result  =result + word.getWordContent()+"/" + word.getNerTag() +"/" + word.getWordPos() + " " + word.getWordIndex() + word.getDependentsString() +"\n" ;
        }

        return result;

        }



    public String deasciify(IDWord word,IDParserTools idParserTool){



            String wordContent = word.getWordContent();
            /*
            String pos1 = idParserTool.tm.analyze(wordContent.replaceAll("i","ı")).get(0).getPos().toString();
            if (pos1!="Unknown") {
            */

                if (!wordContent.contains("a")) {

                    wordContent = wordContent.replaceAll("o", "ö");
                    wordContent = wordContent.replaceAll("u", "ü");

                }
                else{
                    wordContent = wordContent.replaceAll("i", "ı");
                }

                if (wordContent.charAt(0)!='g') {
                    wordContent = wordContent.replace("g", "ğ");
                }

                if (idParserTool.tm.analyze(wordContent).get(0).getPos().toString().equals("Unknown")){
                    wordContent = wordContent.replace("i","ı");
                }


         List<WordAnalysis> analysesList = idParserTool.tm.analyze(wordContent);

        if (analysesList.get(0).getPos().toString().equals("Unknown")){
            return word.getWordContent();
        }
        return wordContent;


    }


    public IDSentence(String sentence,IDParserTools idParserTool){



        C = new Constants();

        subSentences = new ArrayList<String>();




        this.wholeSentence = sentence;





        //nokta ile arasında bosluk olmali
        sentence  = sentence.replace("."," .");

        wordsOfSentence = new ArrayList<IDWord>();

        String[] currentSentenceWords  = sentence.split("\\s+");

        int i = 0;


        SentenceAnalysis currentSentenceAnalysis = idParserTool.analyzer.analyze(sentence);




        //IDNameParser s = new IDNameParser(this.tm);



        for (SentenceAnalysis.Entry myentry:currentSentenceAnalysis){

            //System.out.println(myentry.parses.get(0).getPos());




            //System.out.println(myentry.input+" "+myentry.parses.get(0).getPos());

            IDWord currentWord = new IDWord(myentry.input.toString(),
                    i++,
                    myentry.parses.get(0).getPos().toString(),
                    myentry.parses
            );

            if (myentry.parses.get(0).getSuffixes().contains("Neg")){
                currentWord.setNegative();
            };



            /*
            for (WordAnalysis w:myentry.parses){
                System.out.println(w.getRoot() +" "+s.prepareSuffixTamlamaControl(w.inflectionalGroups.get(0).suffixList));
            }
            */



            if (currentWord.getWordContent().equals("karın") || currentWord.getWordContent().equals("karin")){
                currentWord.setWordRoot("karn");

            }
            else if (currentWord.getWordContent().contains("bulan")){
                currentWord.setWordRoot("bulantı");
            }


            else if (currentWord.getWordContent().equals("mide")){
                currentWord.setWordRoot("karn");
            }
            else if (currentWord.getWordContent().equals("burun")){
                currentWord.setWordRoot("burn");
            }
            else if (currentWord.getWordContent().equals("boyun")){
                currentWord.setWordRoot("boyn");
            }

            else if (currentWord.getWordContent().equals("kulak")){
                currentWord.setWordRoot("kulağ");
            }
            else if (currentWord.getWordContent().equals("basim") || currentWord.getWordContent().equals("bas") ){
                currentWord.setWordRoot("baş");
            }
            else if (currentWord.getWordContent().toLowerCase().equals("göğsüm") || currentWord.getWordContent().toLowerCase().equals("göğsü")){
                currentWord.setWordRoot("göğüs");
            }
            else if (currentWord.getWordContent().toLowerCase().equals("daralıyor") || currentWord.getWordContent().toLowerCase().equals("daralma")){
                currentWord.setWordRoot("dar");
            }
            else if (!myentry.parses.get(0).getPos().toString().equals("Unknown")) {
                String thatRoot = myentry.parses.get(0).getRoot();
                if (thatRoot.equals("kırgın")){
                    currentWord.setWordRoot("hal");
                }
                else if (thatRoot.equals("çınla")){
                    currentWord.setWordRoot("çınl");
                }
                else if (thatRoot.equals("öksürüğ")){
                    currentWord.setWordRoot("öksür");
                }
                else {
                    currentWord.setWordRoot(myentry.parses.get(0).getRoot());
                }
            }

            else{
                //System.out.println("unknown duruma girdim");
                String newRes = deasciify(currentWord,idParserTool);
                //System.out.println(newRes);
                List<WordAnalysis> myAnalysis = idParserTool.tm.analyze(newRes);
                currentWord.setWordRoot(myAnalysis.get(0).getRoot());

            }

            if (currentWord.getWordRoot().equals("yar")){
                currentWord.setWordRoot("yarın");
            }

            if (currentWord.getWordRoot().equals("öksürük")){
                currentWord.setWordRoot("öksür");
            }


            if (!currentWord.getWordContent().equals("_")) {
                if (C.timeContains(currentWord.getWordRoot().toLowerCase().toString())){

                    currentWord.setTimeWordTrue();


                }

                if (C.numericContains(currentWord.getWordRoot().toString())){
                    currentWord.setItNumeric(true);
                }
                wordsOfSentence.add(currentWord);
            }







        }





    }

    public void findMainRoot(IDWord word){


    }

    public void printAllDependeciesOfSentence(){

        for (IDWord word:this.getWordsOfSentence()){

            if (word.getWordRelation()>-1) {

             //   System.out.println(word.getWordContent()
               //         + this.getWordsOfSentence().get(word.getWordRelation()));
            }
        }
    }

    /*
    Uzun iş
     */
    public void setAdjectiveRelations(){

        int sizeOfSentence  =this.getWordsOfSentence().size();




        boolean adjectiveFound=false;

        boolean adjectiveGetNoun=false;

        boolean nounGetAdjective=false;

        int lastAdjectiveNo=-1;

        List<Integer> tempAdjectives = new ArrayList<Integer>();

        for (IDWord word:this.getWordsOfSentence()){

            int i = word.getWordIndex();

            if (i-1<sizeOfSentence && word.getWordPos().equals("Adjective")){



                if (lastAdjectiveNo>-1 && adjectiveGetNoun){



                    lastAdjectiveNo = word.getWordIndex();
                    tempAdjectives.add(lastAdjectiveNo);

                }
                else if (lastAdjectiveNo>-1){
                    lastAdjectiveNo = word.getWordIndex();
                    tempAdjectives.add(lastAdjectiveNo);


                }

                else if (lastAdjectiveNo<=-1){


                    lastAdjectiveNo = word.getWordIndex();
                    tempAdjectives.add(lastAdjectiveNo);



                }

            }
            else if ( (  word.getWordPos().equals("Verb") ||word.getWordPos().equals("Noun")) && lastAdjectiveNo>-1){

                for (Integer positionOfAdjective:tempAdjectives) {

                    //System.out.println(this.getWordsOfSentence().get(positionOfAdjective) + " " + word.getWordContent());
                }

                adjectiveGetNoun = true;

                tempAdjectives.clear();







            }

        }


    }

    public void setRelations(){

        int sizeOfSentence  =this.getWordsOfSentence().size();

        for (IDWord word:this.getWordsOfSentence()){

            //System.out.println(word.getWordRoot());

            if (word.getWordIndex()<sizeOfSentence-1 && word.getWordPos().equals("Adjective") ){



                int i=word.getWordIndex();

                boolean exitThing = true;
                boolean takeNoun = false;
                boolean takeVerb = false;

                while (i<sizeOfSentence-1 && exitThing){


                    IDWord nextWord = this.getWordsOfSentence().get(i + 1);

                    System.out.println(nextWord+" " + nextWord.getWordPos());



                    if (nextWord.getWordPos().equals("Noun") || nextWord.getWordPos().equals("Verb")){
                        System.out.println(word.getWordContent() +"--->" + nextWord.getWordContent() );
                        takeNoun = true;
                    }

                    /*

                     */


                    else if(takeNoun && nextWord.getWordPos().equals("Conjunction")
                            && nextWord.getWordPos().equals("Punctuation")

                            ){
                        exitThing = !exitThing;
                        continue;

                    }
                    else if (nextWord.getWordPos().equals("Conjunction")
                            &&
                            nextWord.getWordPos().equals("Punctuation")
                            ){

                    }
                    else if(takeNoun && nextWord.getWordPos().equals("Adjective")){
                        exitThing = !exitThing;
                        continue;
                    }


                    i++;





                }




            }

        }




    }

    public String toRootFormSentence(){
    
        String result = "";

        for (IDWord word:this.getWordsOfSentence()){
            int wordLength = word.getWordRoot().length();
            if (word.getWordPos().equals("Unknown")
                    &&
                    wordLength>2 &&
                    (word.getWordContent().substring(wordLength-2,wordLength).equals("un") ||
                            word.getWordContent().substring(wordLength-2,wordLength).equals("in"))) {

                System.out.println(word.getWordRoot());
                result = result + word.getWordRoot().substring(0,wordLength-2)  + " ";
            }
            else {
                result = result + word.getWordRoot() + " ";
            }
        }
        return result;
    }




    @Override
    public Iterator iterator() {
        return null;
    }



}

