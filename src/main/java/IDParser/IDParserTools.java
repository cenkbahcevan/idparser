package IDParser;

import DependecyParser.ConllPreparer;
import DependecyParser.DepSentence;
import DependecyParser.Parser;
import zemberek.morphology.ambiguity.Z3MarkovModelDisambiguator;
import zemberek.morphology.analysis.tr.TurkishMorphology;
import zemberek.morphology.analysis.tr.TurkishSentenceAnalyzer;

/**
 * Created by cenk on 24.11.2017.
 */
public class IDParserTools {

    private Z3MarkovModelDisambiguator disambiguator;



    ConllPreparer myPreparer;

    Parser dependecyParser;

    TurkishSentenceAnalyzer analyzer;

    TurkishMorphology tm;

    BayesianOccurenceClassifier classifier;

    ContentPreparer contentPreparer;

    public Constants C;



    public IDParserTools() throws Exception
    {

        this.C = new Constants();


        this.tm = TurkishMorphology.createWithDefaults();

        this.disambiguator = new Z3MarkovModelDisambiguator();

        this.analyzer = new TurkishSentenceAnalyzer(tm, this.disambiguator);

        this.myPreparer = new ConllPreparer("tr-ud-dev.conllu");


        this.dependecyParser = new Parser(this);

        for (DepSentence sentence:this.myPreparer.getDepSentences()) {
            this.dependecyParser.parse(sentence);
        }

        FileReader myReader = new FileReader("training.csv");


        ContentPreparer myPreparer = new ContentPreparer(myReader.getFileContent(),
                "\n","::", true,this);

         this.classifier = new BayesianOccurenceClassifier(myPreparer);

            this.classifier.train();


    }

    public Z3MarkovModelDisambiguator getDisambiguator() {
        return disambiguator;
    }

    public void setDisambiguator(Z3MarkovModelDisambiguator disambiguator) {
        this.disambiguator = disambiguator;
    }

    public TurkishSentenceAnalyzer getAnalyzer() {
        return analyzer;
    }

    public void setAnalyzer(TurkishSentenceAnalyzer analyzer) {
        this.analyzer = analyzer;
    }

    public TurkishMorphology getTm() {
        return tm;
    }

    public void setTm(TurkishMorphology tm) {
        this.tm = tm;
    }
}
