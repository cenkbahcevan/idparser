package IDParser;

import java.io.File;
import java.io.FileNotFoundException;

/**
 * Created by cenk on 10.07.2017.
 */
public class MyFileReader {
    private String fileName;

    private String fileContent;





    public boolean checkIfFileExists() {

        File f = new File(this.fileName);

        return f.isFile();
    }


    public String getFileName() {
        return fileName;
    }

    public String getFileContent() {
        return fileContent;
    }


    MyFileReader(String fileName) throws FileNotFoundException {

        this.fileName = fileName;


        try {

            this.fileContent = new String(java.nio.file.Files.readAllBytes(
                    java.nio.file.Paths.get(this.fileName)));

        }
        catch (java.nio.file.NoSuchFileException e){

            System.out.println("Yok dosya");
            System.exit(0);

        }

        catch (Exception e){
            e.printStackTrace();
        }




    }

}
