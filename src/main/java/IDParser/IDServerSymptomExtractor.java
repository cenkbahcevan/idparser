package IDParser;

import DependecyParser.DepSentence;
import IDParser.IDSentence;
import IDParser.SymptomExtracter;

import java.io.IOException;
import java.net.InetSocketAddress;
import java.nio.ByteBuffer;
import java.nio.channels.SelectionKey;
import java.nio.channels.Selector;
import java.nio.channels.ServerSocketChannel;
import java.nio.channels.SocketChannel;
import java.nio.charset.Charset;
import java.util.*;

/**
 * Created by cenk on 11.07.2017.
 */
public class IDServerSymptomExtractor extends Thread {

    private Selector selector;

    private String adress;

    private int portNo;


    private ServerSocketChannel crunchifySocket;

    private InetSocketAddress crunchifyAddr;

    private int ops;

    private SelectionKey selectKy;



    private SymptomExtracter serverSymptomExtractor;

    IDParserTools idParserTool;


    IDServerSymptomExtractor(String adress, int portNo) throws Exception


    {
        this.idParserTool = null;
        try {
            idParserTool = new IDParserTools();
        }
        catch (Exception e){
            System.exit(-10);
        }


        this.selector = Selector.open(); // selector is open here


        this.crunchifySocket = ServerSocketChannel.open();
        this.crunchifyAddr = new InetSocketAddress(adress, portNo);



        crunchifySocket.bind(crunchifyAddr);

        crunchifySocket.configureBlocking(false);

        int ops = crunchifySocket.validOps();

        this.selectKy = crunchifySocket.register(selector, ops, null);

        this.serverSymptomExtractor = new SymptomExtracter(this.idParserTool);



    }

    public List<String> extractTimeUnitsForRandevu2(IDSentence thisSentence,Map<Integer,List<Integer>> relationMap) {

        List<String> results = new ArrayList<>();

        boolean numericFound = false;
        boolean timeFound = false;

        List<IDWord> words = thisSentence.getWordsOfSentence();

        String tempElement = "";

        for (int i=0;i<words.size();i++){

            IDWord currentWord = words.get(i);

            if (currentWord.isItTimeWord() && numericFound == false && timeFound == false){
                timeFound = true;
                tempElement = tempElement + currentWord.getWordContent();
            }
            else if (currentWord.isItNumeric() && numericFound == false && timeFound == false){
                numericFound = true;
                tempElement = tempElement +  currentWord.getWordContent();
            }
            else if (currentWord.isItTimeWord() && timeFound == true && numericFound == true){
                numericFound = false;
                timeFound = true;
                results.add(tempElement);
                tempElement = "" + currentWord.getWordContent();
            }
            else if (currentWord.isItNumeric() && timeFound == true && numericFound == true){
                timeFound = false;
                numericFound = true;
                results.add(tempElement);
                tempElement = "" + currentWord.getWordContent();
            }

            else if (currentWord.isItNumeric() && numericFound == false ){
                numericFound = true;
                tempElement += " " + currentWord.getWordContent() + " ";
            }
            else if (currentWord.isItTimeWord() && timeFound == false ){
                timeFound = true;
                tempElement += " " + currentWord.getWordContent() + " ";
            }


        }
        results.add(tempElement);
        return  results;

    }

    public void applyUniteDep(){

    }

    public void extractTimeUnitsForRandevu(IDSentence thisSentence,Map<Integer,List<Integer>> relationMap) {

        List<String> results = new ArrayList<>();

        List<IDWord> words = thisSentence.getWordsOfSentence();
        for (int i=0;i<words.size();i++){

            IDWord currentWord = words.get(i);
            /*
            Dependent diffrence from date
             */
            if (currentWord.isItTimeWord()){

                String resultTemp = currentWord.getWordRoot();

                List<Integer> myList = relationMap.get(currentWord.getWordIndex() + 1);

                if (i==0 && words.size()>1 && myList.size()==0){
                    myList.add(words.get(1).getWordIndex());
                }
                //System.out.print(currentWord + "--->");

                for (int j=0;j<myList.size();j++){



                    int indexOfWord = myList.get(j);
                    IDWord currentThisWord = words.get(indexOfWord);
                    //System.out.print(currentThisWord+ " ");
                    if (currentThisWord.isItNumeric()){
                        currentWord.addDependent(currentThisWord);
                  //      System.out.print(" " + currentThisWord + " ");
                    }

                }


            }

            /*
            Vice versa unit is dependet
             */

            if (currentWord.isItNumeric()){

                String resultTemp = currentWord.getWordRoot();

                List<Integer> myList = relationMap.get(currentWord.getWordIndex() + 1);

                if (i==0 && words.size()>1 && myList.size()==0){
                    myList.add(words.get(1).getWordIndex());
                }
                //System.out.print(currentWord + "--->");
                for (int j=0;j<myList.size();j++){



                    int indexOfWord = myList.get(j);
                    IDWord currentThisWord = words.get(indexOfWord);
                    //System.out.print(currentThisWord+ " ");
                    if (currentThisWord.isItTimeWord()){
                        //System.out.print(" " + currentThisWord + " ");
                        resultTemp = resultTemp + " " + currentThisWord.getWordRoot();
                    }

                }

                if (resultTemp.split(" ").length>1) {
                    results.add(resultTemp);
                }
            }




        }
        System.out.println();
    }

    public void analyzeQuery() throws IOException {

        System.out.println("i'm a symptom server and waiting....");
        // Selects a set of keys whose corresponding channels are ready for I/O operations
        selector.select();

        // token representing the registration of a SelectableChannel with a Selector
        Set<SelectionKey> crunchifyKeys = selector.selectedKeys();
        Iterator<SelectionKey> crunchifyIterator = crunchifyKeys.iterator();

        while (crunchifyIterator.hasNext()) {
            SelectionKey myKey = crunchifyIterator.next();

            // Tests whether this key's channel is ready to accept a new socket connection
            if (myKey.isAcceptable()) {
                SocketChannel crunchifyClient = crunchifySocket.accept();

                crunchifyClient.socket().setKeepAlive(true);


                // Adjusts this channel's blocking mode to false
                crunchifyClient.configureBlocking(false);

                // Operation-set bit for read operations
                crunchifyClient.register(selector, SelectionKey.OP_READ);
                System.out.println("Connection Accepted: " + crunchifyClient.getLocalAddress() + "\n");

                // Tests whether this    key's channel is ready for reading
            } else if (myKey.isReadable()) {

                SocketChannel crunchifyClient = (SocketChannel) myKey.channel();




                crunchifyClient.socket().setKeepAlive(true);

                ByteBuffer crunchifyBuffer = ByteBuffer.allocate(256);

                if (crunchifyClient.read(crunchifyBuffer)==-1){
                    crunchifyClient.close();
                    return;
                }

                crunchifyClient.read(crunchifyBuffer);
                String result = new String(crunchifyBuffer.array(),"UTF-8").trim();
                result = result.trim().replaceAll(" +", " ");

                System.out.println("Message received: " + result);

                

                IDSentence thisSentence = new IDSentence(result,idParserTool);

                DepSentence formOfInput;

                Map<Integer,List<Integer>> relationMap;




                CurrentSymptomProfile resultProfile = serverSymptomExtractor.extractPartsFromSentence(thisSentence);

                if (resultProfile.bodyAffectsNew.keySet().size()>1) {

                    try {
                        formOfInput = this.idParserTool.dependecyParser.makeDepSentenceFromRawText(result);

                        relationMap = this.idParserTool.dependecyParser.parseModel(formOfInput);

                        resultProfile.syncWithDependecy(relationMap,thisSentence);

                        /*
                        Time list
                         */

                        String label = thisSentence.getLabelClassified();


                        //extractTimeUnitsForRandevu(thisSentence, relationMap);
                        System.out.println(thisSentence);



                    }catch (Exception e){
                        String symptomProfileResult = resultProfile.toStringJson()+"\n";

                        ByteBuffer output = Charset.forName("UTF-8").encode(symptomProfileResult);




                        crunchifyClient.write(output);
                        return;
                    }

                    Set<String> organs = resultProfile.bodyAffectsNew.keySet();
                    List<String> realOrgans = new ArrayList<>();
                    for (String orga : organs) {
                        realOrgans.add(orga);
                    }


                    for (int i = 0; i < thisSentence.getWordsOfSentence().size(); i++) {

                        IDWord currentWord = thisSentence.getWordsOfSentence().get(i);
                            int firstWordIndex = currentWord.getWordIndex() + 1;
                            for (int j = 0; j < thisSentence.getWordsOfSentence().size(); j++) {
                            IDWord nextParsingWord = thisSentence.getWordsOfSentence().get(j);
                            int secondWordIndex = nextParsingWord.getWordIndex() + 1;



                                if (resultProfile.bodyAffectsNew.containsKey(currentWord.getWordRoot())
                                    && resultProfile.bodyAffectsNew.containsKey(nextParsingWord.getWordRoot())
                                    && i != j
                                    ) {
                                List<Integer> depPart1 = relationMap.get(firstWordIndex);
                                List<Integer> depPart2 = relationMap.get(secondWordIndex);
                                    System.out.println("deppart1:" + depPart1 + "deppart2" + depPart2);
                                if (depPart2 != null && depPart1!= null && depPart2.contains(firstWordIndex)) {

                                    String foundedWordComb = currentWord.getWordRoot() + " " + nextParsingWord.getWordRoot();
                                    System.out.println("Founded Word Comb:" + foundedWordComb);

                                    if (serverSymptomExtractor.getC().multiBodyContains(foundedWordComb)) {
                                        resultProfile.bodyAffectsNew.remove(nextParsingWord.getWordRoot());
                                        resultProfile.bodyAffectsNew.put(foundedWordComb, resultProfile.bodyAffectsNew.get(currentWord.getWordRoot()));
                                        resultProfile.bodyAffectsNew.remove(currentWord.getWordRoot());
                                    }
                                }

                                //dep part 1
                                if (depPart1 != null && depPart1.contains(secondWordIndex)) {

                                    String foundedWordComb = nextParsingWord.getWordRoot() + " " + currentWord.getWordRoot();
                                    System.out.println("Founded Word Comb:" + foundedWordComb);

                                    if (serverSymptomExtractor.getC().multiBodyContains(foundedWordComb)) {
                                        resultProfile.bodyAffectsNew.remove(currentWord.getWordRoot());
                                        resultProfile.bodyAffectsNew.put(foundedWordComb, resultProfile.bodyAffectsNew.get(nextParsingWord.getWordRoot()));
                                        resultProfile.bodyAffectsNew.remove(nextParsingWord.getWordRoot());
                                    }
                                }



                            }



                        }
                    }

                }
        
                String symptomProfileResult = resultProfile.toStringJson()+"\n";

                ByteBuffer output = Charset.forName("UTF-8").encode(symptomProfileResult);




                crunchifyClient.write(output);

            }
            crunchifyIterator.remove();
        }
    }
    
    public void run(){
        try {

            while (true) {
                this.analyzeQuery();


            }
        }
        catch (Exception e){
            e.printStackTrace();
            System.out.println("error");
            try {
                this.crunchifySocket.close();




            } catch (IOException e1) {

                e1.printStackTrace();
            }
        }
    }



}

/*
curl -X POST -H "Content-Type: application/json" -d '{
  "locale":"default",
  "setting_type" : "call_to_actions",
  "thread_state" : "existing_thread",
  "call_to_actions":[
    {
      "type":"postback",
      "title":"Eczane ara 💊",
      "payload":"DEVELOPER_DEFINED_PAYLOAD_FOR_HELP"
    },
    {
      "type":"postback",
      "title":"Haberleri yükle 📰",
      "payload":"DEVELOPER_DEFINED_PAYLOAD_FOR_LATEST_POSTS"
    },
    {
    "type":"postback",
    "title":"Avicena nasıl kullanırım 🤖 ",
    "payload":"DEVELOPER_DEFINED_PAYLOAD_FOR_CLOSEST"
    }

  ]
    }' "https://graph.facebook.com/v2.6/me/thread_settings?access_token=EAATU4NXzrDgBANcwLua6tJlU3xqbbRo1I7jVw0OzmYcaluCEPkdSxcl5k5QfIXaAv4bQL9H3ZC8deTxNfhLqoOZACpZCpiB389nwhSAjJf6ibvjJK2HIIWmjUo4XigZAtEgLXrd4xXh2E85LJaJETIPZBa9bHWJBNpJBFxVWOlcZCsLQVqN12o"
 */