package IDParser;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

import java.util.*;

/**
 * Created by cenk on 14.06.2017.
 */
public class CurrentSymptomProfile {

    private Gson gson;


    Map<String,List<String>> bodyAffects = new LinkedHashMap<String, List<String>>();

    Map<String,Map<String,Map<String,List<String>>>> bodyAffectsNew;

    Map<String,List<String>> bodyAffectsAdjectives = new LinkedHashMap<String, List<String>>();

    List<String> organAndAfects = new ArrayList<>();



    private Set<String> returnOrganList(){
        return this.bodyAffectsNew.keySet();
    }



    CurrentSymptomProfile(){

        this.bodyAffects = new LinkedHashMap<String, List<String>>();

        this.bodyAffectsNew = new HashMap<>();

        this.bodyAffects.put("genel",new ArrayList<String>());
        this.bodyAffects.put("tekli",new ArrayList<String>());

        this.bodyAffectsNew.put("genel",new HashMap<>());
        this.bodyAffectsNew.put("tekli",new HashMap<>());

        this.gson = new GsonBuilder().create();


    }

    public boolean checkIfOrganExist(String organName){

        return bodyAffects.keySet().contains(organName);
    }


    public void putAffectIntoOrgan(String organName,String affectName,String bodyParts)
    {
        this.bodyAffects.get(organName).add(affectName);

        this.bodyAffectsNew.get(organName).put(affectName,new HashMap<>() );

        this.organAndAfects.add(organName+"_" + affectName);



        this.bodyAffectsNew.get(organName).get(affectName).put("adjectives",new ArrayList<>());


        this.bodyAffectsNew.get(organName).get(affectName).put("body_parts",new ArrayList<String>());

        this.bodyAffectsNew.get(organName).get(affectName).put("duration_time",new ArrayList<String>());

        if (!bodyParts.equals(""))
        {
            this.bodyAffectsNew.get(organName).get(affectName).get("body_parts").add(bodyParts);
        }

        System.out.println(this.bodyAffectsNew);



    }

    public void putAffectInOrgan(String organName,String affectName,String adjective){

        this.bodyAffects.get(organName).add(affectName + " " + adjective);


    }


    public void putOrgan(String organName){

        this.bodyAffects.put(organName,new ArrayList<String>());


        this.bodyAffectsNew.put(organName, new HashMap<>());



        System.out.println(this.bodyAffectsNew);


    }

    public void putBodyPartsToAffect(String organName,String affectName,String bodyPart){
        this.bodyAffectsNew.get(organName).get(affectName).
                get("body_parts").add(bodyPart);
    }


    @Override
    public String toString() {
        return bodyAffectsNew.toString();
    }


    public boolean genelContains(String text){
        return this.bodyAffectsNew.get("genel").containsKey(text);
    }

    public String toStringJson(){

        if (this.bodyAffectsNew.containsKey("büyük abdest")){
            this.bodyAffectsNew.put("dışkı",this.bodyAffectsNew.get("büyük abdest"));
            this.bodyAffectsNew.remove("büyük abdest");
        }

        boolean anyKeySymptomAffect = false;

        for (String key:this.bodyAffectsNew.keySet()){
            if (this.bodyAffectsNew.get(key).size()>0){
                anyKeySymptomAffect = true;
            }
        }

        Set<String> tempKeyset = this.bodyAffects.keySet();




        if (anyKeySymptomAffect){

            for (String key:tempKeyset ){
                if (this.bodyAffectsNew.containsKey(key)){
                if (this.bodyAffectsNew.get(key).size()<1 && !key.equals("genel") && !key.equals("tekli")  ){
                    this.bodyAffectsNew.remove(key);

                }
            }}

        }


        return gson.toJson(this.bodyAffectsNew);

    }
    

    public void putAdjective(String organ,String affect,String adjective){
        this.bodyAffectsNew.get(organ).get(affect).get("adjectives").add(adjective);
    }

    public void putDuration(String organ,String affect,String adjective){

            this.bodyAffectsNew.get(organ).get(affect).get("duration_time").add(adjective);
    }

    public String getOrgan(String wordRoot){
        for (String organS:organAndAfects){
            if (organS.contains(wordRoot)){
                return organS.split("_")[0];
            }
        }
        return null;
    }

    public void syncWithDependecy(Map<Integer,List<Integer>> relationMap,IDSentence idSentence){


        //relation map keyleri
        Object[] wordsNo = relationMap.keySet().toArray();

        String timeUnit = "";

        //numaralar foru
       for (int i=0;i<relationMap.size();i++){

           //onun gercek indexi
           int wordNo = (int) wordsNo[i] - 1;

           //o kelimeyi al
           IDWord currentWord = idSentence.getWordsOfSentence().get(wordNo);

           //
           if (this.bodyAffectsNew.containsKey(currentWord.getWordRoot())) {
               //3 ve 5
               List<Integer> myWordsConnected = relationMap.get(wordNo + 1 );
               if (myWordsConnected != null) {

                   for (int k = 0; k < myWordsConnected.size(); k++) {

                       IDWord currentThisWord = idSentence.getWordsOfSentence().get(myWordsConnected.get(k) -1 );

                       if (this.bodyAffectsNew.get(currentWord.getWordRoot()).keySet().size() > 0) {
                           String firstElement = (String) this.bodyAffectsNew.get(currentWord.getWordRoot()).keySet().toArray()[0];


                           if (currentThisWord.getWordPos().equals("Adjective")) {
                               putAdjective(currentWord.getWordRoot(), firstElement, currentThisWord.getWordRoot());
                           }

                           else if(timeUnit.split(" ").length>1){

                               putDuration(currentWord.getWordRoot(),firstElement,timeUnit);
                               timeUnit = "";

                           }

                       }
                   }
               }

           }

           else if (getOrgan(currentWord.getWordRoot()) != null)
           {


               String thisOrgan = getOrgan(currentWord.getWordRoot());

               List<Integer> myWordsConnected = relationMap.get(wordNo + 1);
               //System.out.println(currentWord + " ->" + myWordsConnected );
               if (myWordsConnected != null) {
                   for (int k = 0; k < myWordsConnected.size(); k++) {

                       IDWord currentThisWord = idSentence.getWordsOfSentence().get(myWordsConnected.get(k)-1);



                       if (this.bodyAffectsNew.get(thisOrgan).keySet().size() > 0) {
                           //String firstElement = (String) this.bodyAffectsNew.get(currentWord.getWordRoot()).keySet().toArray()[0];

                           if (currentThisWord.getWordPos().equals("Adjective")) {
                               putAdjective(thisOrgan, currentWord.getWordRoot(), currentThisWord.getWordRoot());
                           }
                           else if(timeUnit.split(" ").length>1){

                               putDuration(thisOrgan,currentWord.getWordRoot(),timeUnit);
                               timeUnit = "";

                           }

                       }
                   }
               }

           }

           else if (currentWord.getWordPos().equals("Adjective")){

               List<Integer> myWordsConnected = relationMap.get(wordNo + 1 );
               for (int index:myWordsConnected){

                   IDWord thisWord = idSentence.getWordsOfSentence().get(index-1);
                   if (this.bodyAffectsNew.containsKey(thisWord.getWordRoot())){
                       String firstElement = (String) this.bodyAffectsNew.get(thisWord.getWordRoot()).keySet().toArray()[0];
                       putAdjective(thisWord.getWordRoot(), firstElement, currentWord.getWordRoot());
                   }

               }

           }

           else if (currentWord.isItTimeWord() || currentWord.isItNumeric()){
               timeUnit = timeUnit + " " + currentWord.getWordRoot();

           }

           }


        System.out.println(timeUnit);
        }

    }




