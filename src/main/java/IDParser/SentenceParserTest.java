package IDParser;

import zemberek.morphology.ambiguity.Z3MarkovModelDisambiguator;
import zemberek.morphology.analysis.SentenceAnalysis;
import zemberek.morphology.analysis.tr.TurkishMorphology;
import zemberek.morphology.analysis.tr.TurkishSentenceAnalyzer;

import java.io.IOException;

/**
 * Created by cenk on 16.06.2017.
 */
public class SentenceParserTest {


    TurkishMorphology tm;

    private Z3MarkovModelDisambiguator disambiguator;

    TurkishSentenceAnalyzer analyzer;



    SentenceParserTest(){
        try {
            this.tm = TurkishMorphology.createWithDefaults();

            disambiguator = new Z3MarkovModelDisambiguator();

            this.analyzer = new TurkishSentenceAnalyzer(this.tm,disambiguator);

        } catch (IOException e) {

        }
    }

    public static void main(String[] args) {


        SentenceParserTest X = new SentenceParserTest();

       SentenceAnalysis mySentence =  X.analyzer.analyze("Başlayışı bana zor geliyor");

        for (SentenceAnalysis.Entry myentry:mySentence) {

            System.out.println(myentry.parses.get(0).getRoot()+ " " + myentry.parses.get(0).getPos());


        }




    }
}
