package IDParser;

import IDParser.IDSentence;
import IDParser.IDWord;
import zemberek.morphology.ambiguity.Z3MarkovModelDisambiguator;
import zemberek.morphology.analysis.SentenceAnalysis;
import zemberek.morphology.analysis.WordAnalysis;
import zemberek.morphology.analysis.tr.TurkishMorphology;
import zemberek.morphology.analysis.tr.TurkishSentenceAnalyzer;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

/**
 * Created by cenk on 28.05.2017.
 */
public class IDNameParser {

    final String[] tamlayanEki = {"ın","in","ün","un"};

    final String[] tamlananEki =  {"ı","i","u","ü"};

    private TurkishMorphology tm;





    IDNameParser(TurkishMorphology myTm){

           tm =  myTm;

    }





    public boolean containsSuffix(String[] mySuffixList,String currentSuffix){
       return  Arrays.asList(mySuffixList).contains(currentSuffix);
    }

    public int checkIfItIsRoot(String myWord,List<WordAnalysis> resultList)

    {
        ArrayList<String> rootList = new ArrayList<String>();

        for (WordAnalysis wordResult:resultList){
            rootList.add(wordResult.getRoot());
        }


        int resultIndexIfTrue = rootList.indexOf(myWord);



        return resultIndexIfTrue!=-1 ? resultIndexIfTrue : 0;

    }

    public String prepareSuffixTamlamaControl(List<WordAnalysis.SuffixData> resultData) {


        String ek="";
        if (resultData.size() > 0) {

            for (int i=resultData.size()-1;i>=0;i--) {

                String tempRes = resultData.get(i).lex.toString().toLowerCase();
                if (!tempRes.equals("")) {

                    ek = resultData.get(i).lex.toString().toLowerCase();
                    break;
                }
            }

            if (ek.length() < 2) {
                ek = "";
            } else {
                ek = ek.substring(2);
            }


        }
        return ek;
    }

    public boolean zincerlemeTamlamaKontrol(IDSentence sentence,int indexOfWord){


        for (IDWord word:sentence.getWordsOfSentence()){

            List<WordAnalysis> tempRes = tm.analyze(word.getWordContent());

            List<WordAnalysis.SuffixData> myList = tempRes.get(0).inflectionalGroups.get(0).suffixList;

            String ek = prepareSuffixTamlamaControl(myList);

            System.out.println(ek);



        }


        return  true;
    }


    public int isimTamlamaKontrol(IDSentence sentence, int indexOfWord) {


        IDWord firstWord = sentence.getWordsOfSentence().get(indexOfWord);
        IDWord secondWord = sentence.getWordsOfSentence().get(indexOfWord + 1);

        List<WordAnalysis.SuffixData> result1Suffixes = new ArrayList<WordAnalysis.SuffixData>();
        List<WordAnalysis.SuffixData> result2Suffixes = new ArrayList<WordAnalysis.SuffixData>();




        try {


            /*
            Disambuguationi cumle cumle analiz ile cozuyorum
             */

            Z3MarkovModelDisambiguator disambiguator = new Z3MarkovModelDisambiguator();

            TurkishSentenceAnalyzer analyzer = new TurkishSentenceAnalyzer(tm, disambiguator);

            SentenceAnalysis analysis  = analyzer.analyze(sentence.getWholeSentence());

            result1Suffixes= analysis.getEntry(0).parses.get(0).inflectionalGroups.get(0).suffixList;

            result2Suffixes = analysis.getEntry(1).parses.get(0).inflectionalGroups.get(0).suffixList;

            System.out.println(analysis.getEntry(1).parses.get(0).getRoot());

        }



        catch(Exception e){

            /*
            Morpholojik altyapı çalışmıyor.
             */
            System.exit(-10);

        }



        //bu gelen eki bana listemin icinde kontrol edebilecegim sekilde hazirliyor
        String ek1 = prepareSuffixTamlamaControl(result1Suffixes);
        String ek2 = prepareSuffixTamlamaControl(result2Suffixes);

        System.out.println("1.nci kelimenin eki:"+ek1);

        System.out.println("2.nci kelimenin eki:"+ek2);


        if (containsSuffix(tamlayanEki,ek1) && containsSuffix(tamlananEki,ek2)){
            System.out.println("Cevap 1 belirtili");
            return 1;
        }
        else if(containsSuffix(tamlananEki,ek2)){
            System.out.println("Cevap 1 belirtisiz");
            return 2;
        }
        else if (ek1.equals("") && ek2.equals("")){
            System.out.println("Cevap 1 takısız");
            return 3;
        }


        return -1;



    }







}
