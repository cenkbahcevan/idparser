package IDParser;

import IDParser.Constants;
import IDParser.CurrentSymptomProfile;
import IDParser.IDSentence;
import IDParser.IDWord;
import zemberek.morphology.ambiguity.Z3MarkovModelDisambiguator;
import zemberek.morphology.analysis.tr.TurkishSentenceAnalyzer;

import java.util.*;

/**
 * Created by cenk on 7.06.2017.
 */
public class SymptomExtracter {


    IDParserTools idParserTool;

    private  Z3MarkovModelDisambiguator disambiguator;

    TurkishSentenceAnalyzer analyzer;



    private Map<String,List<String>> bodyPartsOfParts;

    private Map<String,String> organSynonymsDictionary;

    Constants C;




    public Constants getC() {
        return C;
    }

    private int lastOrganIndexForParts = -1;

    private static final Map<String, String> zemberekFailMap;

    static {
        zemberekFailMap = new HashMap<>();
            zemberekFailMap.put("genizim","geniz");
            zemberekFailMap.put("genzim","geniz");
            zemberekFailMap.put("geniz","geniz");
            zemberekFailMap.put("basim","baş");
    }


    final String[] organSynonyms = {
            "kafa:baş",
            "mide:karn",
            "idrar:dışkı",
            "sik:penis",
            "popo:kalça",
            "omuz:sırt",
            "kalp:göğüs",
            "meme:göğüs",
            "hapşırık:öksürük",
    };



    SymptomExtracter(IDParserTools idParserTool){

        this.idParserTool = idParserTool;

        this.C = new Constants();

        this.bodyPartsOfParts = new HashMap<String,List<String> >();

        this.organSynonymsDictionary = new HashMap<String,String>();

        for (String word:organSynonyms){
            if (word.contains(":")){
                String[] currentWordAndSyn = word.split(":");
                this.organSynonymsDictionary.put(currentWordAndSyn[0],currentWordAndSyn[1]);
            }
        }


        List<String> arr = new ArrayList<String>();
        arr.add("alt");
        arr.add("üst");
        arr.add("ön");
        arr.add("arka");

        this.bodyPartsOfParts.put("baş",arr);

        List<String> arr2 = new ArrayList<String>();
        arr2.add("yan");
        arr2.add("sağ");
        arr2.add("sol");
        this.bodyPartsOfParts.put("burn",arr2);

        try {

            this.disambiguator = new Z3MarkovModelDisambiguator();

            this.analyzer = new TurkishSentenceAnalyzer(this.idParserTool.getTm(), this.disambiguator);
        }
        catch (Exception myExcp){
            System.exit(0);
        }
    }

    public void insertIntoCurrentProfileMap(){

    }

    public String changeUnvowelLastLetter(String wordRoot){
        if (wordRoot.charAt(wordRoot.length()-1)=='b'){
            return wordRoot.substring(0,wordRoot.length()-1)+'p';
        }
        else if (wordRoot.charAt(wordRoot.length()-1)=='ğ' ||
                wordRoot.charAt(wordRoot.length()-1)=='g'
                ){

            return wordRoot.substring(0,wordRoot.length()-1)+'k';

        }
        else if (wordRoot.charAt(wordRoot.length()-1)=='c'){

            return wordRoot.substring(0,wordRoot.length()-1)+'ç';

        }



        return "";


    }

    public HashSet<String> typeOfCosts(IDSentence sentence){

        HashSet<String> resultList =  new HashSet<String>();

        for (IDWord word:sentence.getWordsOfSentence()){

            if (C.ucretContains(word.getWordRoot())){
                resultList.add(word.getWordRoot());
            }

        }

        return resultList;

    }


    public CurrentSymptomProfile extractPartsFromSentence(IDSentence sentence){




        Map<String,List<String>> myOrgaAdj;

        boolean bodyPartFlag=false;

        boolean bodyPartAnotherFlag=false;

        boolean affectedPartFlag=false;

        boolean affectedPartAnotherFlag=false;

        String unmatchedPart = "";

        String temp="";


        Map<String,List<String>> tempMap = new LinkedHashMap<String, List<String>>();

        CurrentSymptomProfile thisSentenceProfie = new CurrentSymptomProfile();

        /*
        General olan var
         */

        tempMap.put("genel",new ArrayList<String>());

        tempMap.put("tekli",new ArrayList<String>());

        String tempEtki = "";

        List<String> tempEtkiList = new ArrayList<String>();

        List<String> tempPartList = new ArrayList<String>();

        List<String> tempOrgan = new ArrayList<String>();

        int wordPos = 0;

        int tempOrganNo = 0;

        int unmarkedOrganNo = 0;

        int lastOrganNo = -2;

        int lastAffectNo=2;

        boolean organTemp=false;

        boolean affectedTemp=false;

        boolean afterOrgan=false;

        String tempAdjectives="";

        boolean synonymOrgan = false;

        String beforeOrganDikkat = "";
        int beforeOrganIndex = -1;

        boolean multiBody = false;
        String multiBodyStr = "";


        String result  = this.idParserTool.classifier.predict(sentence);

        sentence.setLabelClassified(result);

        for (IDWord word:sentence.getWordsOfSentence()){


            if (C.ucretContains(word.getWordRoot()) && thisSentenceProfie.genelContains("ücret")){

                thisSentenceProfie.putBodyPartsToAffect("genel","ücret",word.getWordRoot());
            }

            if (C.thankContains(word.getWordContent().toLowerCase())){

                thisSentenceProfie.putAffectIntoOrgan("genel","tesekkur","");

            }
            else if (C.greetingContains(word.getWordContent().toLowerCase())){
                thisSentenceProfie.putAffectIntoOrgan("genel","selam","");
            }
            else if (C.understandContains(word.getWordContent().toLowerCase())){
                thisSentenceProfie.putAffectIntoOrgan("genel","anladim","");
            }
            else if (C.farewellContains(word.getWordContent().toLowerCase())){
                thisSentenceProfie.putAffectIntoOrgan("genel","veda","");
            }
            else if (C.appointmentContains(word.getWordContent().toLowerCase())){
                thisSentenceProfie.putAffectIntoOrgan("genel","randevu","");
            }

            System.out.println("Etki:"+ result );
        if (result.equals("ücret")){
            thisSentenceProfie.putAffectIntoOrgan("genel","ücret","");
        }
        else if (result.equals("yardim")){
            thisSentenceProfie.putAffectIntoOrgan("genel","yardim","");
        }
        else if (result.equals("personal")){
            thisSentenceProfie.putAffectIntoOrgan("genel","personal","");
        }
        else if (result.equals("cinsel")){
            thisSentenceProfie.putAffectIntoOrgan("genel","cinsel","");
        }
        else if (result.equals("psikoloji")){
            thisSentenceProfie.putAffectIntoOrgan("genel","psikoloji","");
        }
        else if (result.equals("kilo")){
            thisSentenceProfie.putAffectIntoOrgan("tekli","kilo","");
        }
        else if (result.equals("veda")){
            thisSentenceProfie.putAffectIntoOrgan("genel","veda","");
        }
        else if (result.equals("ilac")){
            thisSentenceProfie.putAffectIntoOrgan("genel","ilac","");
        }
        else if (result.equals("hakaret")){
            thisSentenceProfie.putAffectIntoOrgan("genel","hakaret","");
        }
        else if (  result.equals("hamile") || result.equals("sigara") || result.equals("cilt") || result.equals("kanser")){
            thisSentenceProfie.putAffectIntoOrgan("genel",result,"");
        }
        else if (result.equals("merhaba") &&     !thisSentenceProfie.genelContains("selam")){
            thisSentenceProfie.putAffectIntoOrgan("genel","selam","");
            }
        else if (result.equals("adet")){
            thisSentenceProfie.putAffectIntoOrgan("tekli",result,"");
        }












            








            //System.out.println(lastOrganNo);

            organTemp = true;

            synonymOrgan = organSynonymsDictionary.containsKey(word.getWordRoot());

            boolean zemberekWrongRoot = zemberekFailMap.containsKey(word.getWordContent().toLowerCase());

            if(C.organContains(word.getWordRoot()) ||
                    C.organContains(changeUnvowelLastLetter(word.getWordRoot()))
                    ||
                   synonymOrgan || zemberekWrongRoot


                    ) {





                if (organTemp && affectedTemp){
                    afterOrgan=true;
                    organTemp=false;
                    affectedTemp=false;
                }


                tempOrganNo++;





                if (word.getWordIndex()>0 && lastOrganNo>=0 ){

                    IDWord wordBefore = sentence.getWordsOfSentence().get(lastOrganNo);

                    System.out.println("oldum"+wordBefore.getWordContent() + " "  + afterOrgan);

                    if((C.organContains(wordBefore.getWordRoot()) ||
                            C.organContains(changeUnvowelLastLetter(wordBefore.getWordRoot())))
                            && !afterOrgan

                            ) {



                        //lastOrganNo = wordBefore.getWordIndex();
                    }
                    else{
                        System.out.println("bir else in");
                        lastOrganNo = word.getWordIndex();
                    }



                }
                else {


                    lastOrganNo = word.getWordIndex();
                }





                if (synonymOrgan){
                    tempMap.put(organSynonymsDictionary.get(word.getWordRoot()), new ArrayList<String>());

                    thisSentenceProfie.putOrgan(organSynonymsDictionary.get(word.getWordRoot()));

                    //synonymOrgan = false;

                }

                else if (zemberekWrongRoot){



                    tempMap.put(zemberekFailMap.get(word.getWordContent().toLowerCase()),new ArrayList<>());

                    thisSentenceProfie.putOrgan(zemberekFailMap.get(word.getWordContent().toLowerCase()));


                }
                else if (!tempMap.containsKey(word.getWordRoot())) {



                    


                    tempMap.put(word.getWordRoot(), new ArrayList<String>());

                    thisSentenceProfie.putOrgan(word.getWordRoot());
                }



                //o organın belirteclerini hangi kısmı




                    for (String element:tempEtkiList) {

                        boolean firstPoss = tempMap.containsKey(word.getWordRoot());
                        boolean secondPoss = tempMap.containsKey(word.getWordContent().toLowerCase());

                        if (firstPoss) {
                            tempMap.get(word.getWordRoot()).add(element);
                        }
                        else{
                            String myWord = zemberekFailMap.get(word.getWordContent().toLowerCase());
                            if (tempMap.containsKey(myWord)) {
                                tempMap.get(myWord).add(element);
                            }
                        }

                        if (synonymOrgan){

                            thisSentenceProfie.putAffectIntoOrgan(
                                    organSynonymsDictionary.get(word.getWordRoot()), element,unmatchedPart);

                            unmatchedPart = "";
                            synonymOrgan = false;

                        }
                        else if (zemberekWrongRoot){

                            thisSentenceProfie.putAffectIntoOrgan(
                                    zemberekFailMap.get(word.getWordContent().toLowerCase()), element,unmatchedPart);

                            unmatchedPart = "";
                            zemberekWrongRoot = false;


                        }
                        else {

                            thisSentenceProfie.putAffectIntoOrgan(word.getWordRoot(), element ,unmatchedPart);
                            unmatchedPart = "";
                        }
                    }
                    tempEtkiList = new ArrayList<String>();



                temp += word.getWordRoot()+" ";


            //    System.out.println("organ ->" + word.getWordContent() + " " + word.getWordPos());
            }






            else if (C.affectContains(word.getWordRoot()) || (word.isNegative() && C.negativeAffectContains(word.getWordRoot()))){
                System.out.println("etkilenme->" + word.getWordContent() + " " + word.getWordPos());



                affectedTemp=true;




                int lastElementIndex = tempMap.keySet().size()-1;

               // 1 çünkü bizde genelde var
                if (lastElementIndex>=2   ){


                    for (int i=word.getWordIndex()-1;i>lastOrganNo-1;i--) {


                        /*
                        Kelimenin indexi 0 ise oncesi yoktur bama
                         */
                        if (i<0){
                            break;
                        }


                        boolean currSynonym = organSynonymsDictionary.containsKey(sentence.getWordsOfSentence().get(i).getWordRoot());
                        boolean currZemberekFail = zemberekFailMap.containsKey(sentence.getWordsOfSentence().get(i).getWordContent().toLowerCase());

                        String myKey;

                        if (tempMap.containsKey(sentence.getWordsOfSentence().get(i).getWordRoot())
                                || currSynonym || currZemberekFail
                                ) {



                            if (currSynonym) {

                                myKey = organSynonymsDictionary.get(sentence.getWordsOfSentence().get(i).getWordRoot());


                                tempMap.get(myKey).add(word.getWordRoot());
                            }
                            else if (currZemberekFail){
                                myKey = zemberekFailMap.get(sentence.getWordsOfSentence().get(i).getWordContent().toLowerCase());


                                tempMap.get(myKey).add(word.getWordRoot());
                            }




                            else{

                                System.out.println("son else içindeyim");

                                 myKey = sentence.getWordsOfSentence().get(i).getWordRoot();


                                tempMap.get(myKey).add(word.getWordRoot());

                            }





                            if (tempAdjectives.equals("")) {

                                System.out.println("if koyma durumu");


                                thisSentenceProfie.putAffectIntoOrgan(myKey, word.getWordRoot(),unmatchedPart);
                                unmatchedPart = "";
                            }
                            else{

                                System.out.println("else durumu");
                                //thisSentenceProfie.putAffectIntoOrgan(myKey, tempAdjectives+ " " + word.getWordRoot());
                                thisSentenceProfie.putAffectIntoOrgan(myKey, word.getWordRoot(),unmatchedPart);
                                unmatchedPart = "";

                                tempAdjectives = "";

                            }





                        }
                        else  if (multiBody){

                            System.out.println("Multi body içindeyim");


                            myKey = multiBodyStr;
                            multiBody = false;
                            thisSentenceProfie.putAffectIntoOrgan(myKey, word.getWordRoot(),"");


                        }

                    }


                }
                else{
                    tempEtki = word.getWordRoot();
                    System.out.println("tempEtki" + tempEtki);
                    tempEtkiList.add(word.getWordRoot());
                }


                temp += word.getWordRoot()+" ";
            }

            else if(C.generalAffectsContains(word.getWordRoot())){


                tempMap.get("tekli").add(word.getWordContent());

                if (tempAdjectives.equals("")) {
                    thisSentenceProfie.putAffectIntoOrgan("tekli", word.getWordRoot(),"");
                }
                else{

                    thisSentenceProfie.putAffectIntoOrgan("tekli", tempAdjectives+ " " + word.getWordRoot(),"");
                }

            }

            else if (word.getWordRoot().equals("yorgun")){
                tempMap.get("tekli").add("hal");

                thisSentenceProfie.putAffectIntoOrgan("tekli","hal","");

            }

            else if (word.getWordIndex()>0){

                IDWord currentWord = word;
                IDWord beforeWord = sentence.getWordsOfSentence().get(word.getWordIndex() -1);

                String wordComb = beforeWord.getWordRoot() + " " + currentWord.getWordRoot();

                if (C.multiBodyContains(wordComb)) {
                    thisSentenceProfie.putOrgan(wordComb);
                    multiBody = true;
                    multiBodyStr = wordComb;
                    tempMap.put(wordComb,new ArrayList<>());
                }


            }




            wordPos++;





        }
        System.out.println("bittim:" + thisSentenceProfie);

            /*
            Too many if
             */







        return thisSentenceProfie;




    }





    public static void main(String[] args) throws Exception {



        IDParserTools idParserTools = new IDParserTools();

        SymptomExtracter extracter = new SymptomExtracter(idParserTools);

        IDSentence mySentence = new IDSentence("başım ağrıyor ve burnum çok şiddetli akıyors",idParserTools);

        extracter.extractPartsFromSentence(mySentence);

       // IDParser.MyTurkishMorphology mine = new IDParser.MyTurkishMorphology();

      //  IDParser.SymptomExtracter d = new IDParser.SymptomExtracter(mine);


        //DSentence mySentence = new IDParser.IDSentence("Başım ağrıyor ve burnum akıyor",mine.tm);

        //mySentence.setAdjectiveRelations();

     //   mySentence.printAllDependeciesOfSentence();




       // d.extractPartsFromSentence(mySentence);




    }

}
