package IDParser;

import zemberek.morphology.analysis.WordAnalysis;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by cenk on 28.05.2017.
 */
public class IDWord {

    private String wordContent;

    private String wordPos;

    private List<IDWord> dependents;

    private String nerTag = "O";

    public String getNerTag() {
        return nerTag;
    }

    public void setNerTag(String nerTag) {
        this.nerTag = nerTag;
    }

    private boolean isItTimeWord = false;

    private List<String> wordSuffixCandidates;

    private List<String> wordPosCandidates;

    private int wordIndex;

    private int wordRelation = -1;

    private String wordTag;

    private String wordRoot;

    private boolean isItNumeric;

    private boolean isNegative;

    public void setItNumeric(boolean itNumeric) {
        isItNumeric = true;
    }

    public boolean isItNumeric() {
        return isItNumeric;
    }

    public void setTimeWordTrue(){
        this.isItTimeWord = true;
    }

    public void setNegative() {
        this.isNegative = true;
    }

    public boolean isNegative() {
        return isNegative;
    }

    private List<String> getSuffixCandidates(List<WordAnalysis> wordAnalysis) {

        List<String> result = new ArrayList<String>();
        boolean noSuffix = true;
        for (WordAnalysis analysis : wordAnalysis) {


            List<String> currentSuffixesList = analysis.suffixSurfaceList();

            //System.out.println(currentSuffixesList);

            for (String currentSuffix : currentSuffixesList) {



                if (!currentSuffix.equals("")
                        && !result.contains(currentSuffix)) {
                    noSuffix = false;
                    result.add(currentSuffix);
                }
            }
            if (noSuffix && !result.contains("nosuffix")) {
                result.add("nosuffix");
            }


        }

        return result;
    }

    public boolean isItTimeWord() {
        return isItTimeWord;
    }

    private List<String> getPosCandidates(List<WordAnalysis> wordAnalyses){

        List<String> result = new ArrayList<String>();

        for (WordAnalysis analysis:wordAnalyses){

            String currentWordPos = analysis.getPos().toString();
            if (!result.contains(currentWordPos))
                result.add(analysis.getPos().toString());
        }

        return result;
    }

    public IDWord(String wordContent, int wordIndex,String wordPos,List<WordAnalysis> wordAnalyses) {
        this.wordContent = wordContent;
        this.wordIndex = wordIndex;
        this.wordPos = wordPos;


        this.dependents = new ArrayList<>();

        this.wordSuffixCandidates = this.getSuffixCandidates(wordAnalyses);
        this.wordPosCandidates = this.getPosCandidates(wordAnalyses);
    }

    public void setWordRoot(String wordRoot){
        this.wordRoot =  wordRoot;
    }

    public String getWordRoot(){
        return this.wordRoot;
    }

    public String getWordContent() {
        return wordContent;
    }

    public String getWordPos(){
        return this.wordPos;
    }

    public void setWordContent(String wordContent) {
        this.wordContent = wordContent;
    }

    public int getWordIndex() {
        return wordIndex;
    }

    public void setWordIndex(int wordIndex) {
        this.wordIndex = wordIndex;
    }

    public int getWordRelation() {
        return wordRelation;
    }

    public void setWordRelation(int wordRelation) {
        this.wordRelation = wordRelation;
    }

    public String getWordTag() {
        return wordTag;
    }

    public void setWordTag(String wordTag) {
        this.wordTag = wordTag;
    }

    public String toString(){
        return this.wordContent;
    }

    public List<String> getWordSuffixCandidates() {
        return wordSuffixCandidates;
    }

    public List<String> getWordPosCandidates() {
        return wordPosCandidates;
    }

    public void addDependent(IDWord currentWord){
        this.dependents.add(currentWord);
    }

    public String getDependentsString() {
        String startStr = "[";

        for (IDWord word:dependents){
            startStr = startStr + word + " ";
        }

        return startStr + "]";
    }
}
