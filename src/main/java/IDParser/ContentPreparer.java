package IDParser;

import java.io.FileNotFoundException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.Stream;

/**
 * Created by cenk on 24.06.2017.
 */
public class ContentPreparer {


        private String documentDelimeter="\n";

        private String elementDelimeter="\t";

        private boolean isFirstLineColumnNames;

        private List<List<String>> preparedContent;

        private List<IDSentence> sentences;

        private List<String> labels;

        public boolean isFirstLineColumnNames() {
            return isFirstLineColumnNames;
        }

        public void setDocumentDelimeter(String documentDelimeter) {
            this.documentDelimeter = documentDelimeter;
        }

        public void setElementDelimeter(String elementDelimeter) {
            this.elementDelimeter = elementDelimeter;
        }

        public void setFirstLineColumnNames(boolean firstLineColumnNames) {
            isFirstLineColumnNames = firstLineColumnNames;
        }

        public ContentPreparer(String content,String documentDelimeter, String elementDelimeter, boolean isFirstLineColumnNames,
                               IDParserTools idParserTool) {
            this.documentDelimeter = documentDelimeter;
            this.elementDelimeter = elementDelimeter;
            this.isFirstLineColumnNames = isFirstLineColumnNames;

            this.sentences = new ArrayList<>();

            this.labels = new ArrayList<>();


            String[] documentSplitted = content.split(this.documentDelimeter);

            for (String line : documentSplitted){

                String[] lineSplitted = line.split(this.elementDelimeter);
                if (lineSplitted.length>1){
                    //System.out.println(Arrays.toString(lineSplitted));
                    IDSentence currentSentence = new IDSentence(lineSplitted[0],idParserTool);
                    String currentLabel =  lineSplitted[1];

                    this.sentences.add(currentSentence);
                    this.labels.add(currentLabel);
                }
            }


      /*
      for (List row:this.preparedContent){
          for (Object element:row){
              System.out.print(element+" ");

          }
          System.out.println();
      }
      */


        }

        public List<List<String>> getPreparedContent() {
            return preparedContent;
        }

    public List<IDSentence> getSentences() {
        return sentences;
    }

    public List<String> getLabels() {
        return labels;
    }

        /*
    public static void main(String[] args) {

        try {
            FileReader r = new FileReader("test.csv");

            ContentPreparer X = new ContentPreparer(r.getFileContent(),
                    "\n",
                    ","
                    ,false);


        }
        catch (FileNotFoundException e){
            e.printStackTrace();
        }

    }

*/

    }
